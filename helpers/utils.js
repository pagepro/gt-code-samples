
export const isDev = () => process.env.NODE_ENV !== 'production'

const date = date => {
  return date ? new Date(date) : new Date()
}

export const preventDefault = (callback, withoutPropagation = false) => event => {
  if (event && event.preventDefault) {
    event.preventDefault()
  }

  if (event && withoutPropagation && event.stopPropagation) {
    event.stopPropagation()
  }

  if (callback) {
    callback()
  }
}

export const getErrorDataFromResponse = error => {
  if (error && error.response && error.response.data) {
    return error.response.data
  }
}

export const hasAllParams = data => {
  return getErrorDataFromResponse(data).every(err => !!err.param)
}

export const getErrorMessages = (data, isParsed) => {
  // errorception
  const errors = isParsed ? data : getErrorDataFromResponse(data)
  if (errors) {
    return errors.map(error => {
      return error.errors.map(errDetails => {
        return errDetails.message
      })
    }).join(' ')
  }
}

export const formatToNumber = (value = String.empty) => {
  if (!value) return ''
  const maxSaveIntLength = Number.MAX_SAFE_INTEGER.toString().length - 1
  value = value.toString().substr(0, maxSaveIntLength)
  const parsedValue = parseInt(`${value}`.replace(/\D/g, String.empty))

  if (isNaN(parsedValue)) return String.empty

  return parsedValue.toString()
}

export const formatToPrice = value => {
  // price should have a comma as a separator for thousands (10,000 = ten thousand)
  /*  @todo once we begin implementation for other languages (i.e. end of the year)
      we'll have to add additional formatting options default for a set language
  */
  if (!value) {
    return ''
  }
  const numberValue = formatToNumber(value)
  const stringValue = `${numberValue}`
  const backwards = stringValue.split('').reverse()
  const withSpaces = backwards.map((item, index) => {
    return (index % 3 === 0 && index > 0) ? `${item},` : item
  })
  return withSpaces.reverse().join('')
}

export const reduceArrayToObject = keySelector => (result, item) => ({
  ...result,
  [keySelector(item)]: item
})

export const reduceAndFilterObject = (object, keySelector, filterMethod = () => true) => {
  const items = Object.values(object)
  return items.reduce((result, item) => {
    if (!filterMethod(item)) {
      return result
    }
    return {
      ...result,
      [keySelector(item)]: item
    }
  }, {})
}

export const sortDatesAsc = keySelector => (itemA, itemB) => {
  return date(keySelector(itemA)) - date(keySelector(itemB))
}

export const sortDatesDesc = keySelector => (itemA, itemB) => {
  return date(keySelector(itemB)) - date(keySelector(itemA))
}