import guid from 'uuid/v4'

class Guid {
  constructor (guid = Guid.empty) {
    this.guid = guid
  }

  static newGuid () {
    return new Guid(guid())
  }

  static get empty () {
    return '00000000-0000-0000-0000-000000000000'
  }

  toString () {
    return this.guid
  }
}

export default Guid
