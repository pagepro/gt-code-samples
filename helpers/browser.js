import ReactDOM from 'react-dom'

export const scrollToElement = ({
  selectorOrElement,
  offset = 0,
  scrollingContainer = window
}) => {
  const element = typeof selectorOrElement === 'string'
    ? document.querySelector(selectorOrElement)
    : selectorOrElement
  const reactNode = ReactDOM.findDOMNode(element)

  if (reactNode) {
    window.requestAnimationFrame(() => {
      const offsetBody = document.body.getBoundingClientRect().top
      const offsetTop = scrollingContainer === window
        ? (element.getBoundingClientRect().top - offsetBody - offset)
        : (element.offsetTop - offset)

      scrollingContainer.scroll(0, offsetTop)
    })
  }
}

export const dispatchGlobalEvent = (
  eventName, {
  bubbles = false,
  cancelable = false,
  detail = undefined,
  composed = false
} = {}) => {
  let event = null
  if (typeof window.Event === 'function') {
    event = new window.CustomEvent(eventName, {
      bubbles,
      cancelable,
      composed,
      detail
    })
  } else {
    event = document.createEvent('CustomEvent')
    event.initCustomEvent(eventName, bubbles, cancelable, detail)
  }

  window.dispatchEvent(event)

  return event
}

export const isIOS = () => /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream

export const scrollTop = () => {
  window.requestAnimationFrame(() => {
    window.scroll(0, 0)
  })
}
