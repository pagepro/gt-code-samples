import axios from 'axios'
import messagesFull from 'common/messages'
import { pushErrorNotification } from 'admin/app/AppActions'
import {
  combineErrorMessage
} from 'admin/forms/validators'

const tryGetErrorMessage = data => {
  if (data.message) {
    return data.message.trim()
  }

  try {
    return data
      .reduce((result, { errors, param }) => [
        ...result,
        combineErrorMessage(errors, param)
      ], [])
      .join(' ')
      .trim()
  } catch (ex) {
    return null
  }
}

export default function configureErrorInterceptor (store) {
  axios.interceptors.response.use(response => response, error => {
    // these will need a refactoring for sure...
    const {
      response: {
        data,
        status,
        config
      } = {},
      message
    } = error || {}
    const {
      dispatch,
      getState
    } = store
    const responseDataErrorMessage = tryGetErrorMessage(data)
    const { thereWasAnErrorExecutingAction } = messagesFull[getState().setup.language]
    const displayErrorMessage = `${responseDataErrorMessage || (thereWasAnErrorExecutingAction + ' ' + message)}`
    const errorInterceptor = config.errorInterceptor || {}

    // When we need to disable displaying errors
    if (errorInterceptor.skip) {
      return Promise.reject(error)
    }
    // when there is a network error, we can determine it by checking whether its response is undefined
    const hasResponse = (typeof response !== 'undefined')
    const hasErrorMessage = hasResponse ? !!data : false
    // in case there's a bad request or unauthorized|forbidden, but no actual error message
    // others(i.e. with message) should be handled already individually
    // any other errors are gucci
    const isUnauthorized = status === 401 || status === 403
    const isBadRequest = status === 400
    // if there is no error message at all, pop the notification as well
    if (!isUnauthorized && ((hasResponse && !isBadRequest) || !hasErrorMessage)) {
      if (typeof errorInterceptor.closeOnLeave === 'undefined') {
        dispatch(pushErrorNotification(displayErrorMessage))
      } else {
        dispatch(pushErrorNotification(displayErrorMessage, errorInterceptor.closeOnLeave))
      }
    }
    return Promise.reject(error)
  })
}

export const skipInterceptorObject = Object.freeze({
  errorInterceptor: Object.freeze({
    skip: true
  })
})
