import React, { Fragment } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Header from 'admin/user/UserRolesHeader'
import Table from 'admin/tables/Table'
import tableColumns from 'admin/user/UserRolesColumns'
import { USER_ROLES } from 'admin/setup/tableTypes'
import omit from 'lodash/omit'
import {
  getUsers,
  saveUser,
  saveNewUser,
  countSubscriptionUsers
} from 'admin/user/UserRolesActions'
import {
  openEdit,
  saveEdit,
  editTableData,
  updateTableItem,
  addRow
} from 'admin/tables/TablesActions'
import {
  openConfirmationModal,
  openInfoModal
} from 'admin/modals/ModalsActions'
import {
  getBranchesList
} from 'admin/branches/Actions'
import IDTokenInfo from 'utils/IDTokenInfo'
import BigLoader from 'common/BigLoader'
import {
  logout
} from 'admin/signin/SigninActions'
import withLanguage from 'common/withLanguage'
import { NEW_ITEM_ID } from './utils'
import classnames from 'classnames'
import { getMaxSubscriptionCount } from 'admin/companyDetails/CompanyDetailsReducer'

class UserRoles extends React.PureComponent {
  constructor (props) {
    super(props)

    this.state = {
      isRequiredDataFetched: false
    }
    this.FIELDS = {
      admin: 'isAdmin',
      subscription: 'isSubscriptionUser',
      enabled: 'isEnabled',
      branchId: 'agencyBranchId',
      branchName: 'agencyBranchName'
    }

    this.edit = this.edit.bind(this)
    this.toggleUser = this.toggleUser.bind(this)
    this.toggleLicense = this.toggleLicense.bind(this)
    this.save = this.save.bind(this)
    this.updateIsAdminField = this.updateIsAdminField.bind(this)
    this.updateBranchField = this.updateBranchField.bind(this)
    this.updateTextField = this.updateTextField.bind(this)
    this.openModal = this.openModal.bind(this)
    this.saveAndUpdate = this.saveAndUpdate.bind(this)
    this.getBranchseSelectOptions = this.getBranchseSelectOptions.bind(this)
    this.addUser = this.addUser.bind(this)
    this.saveNew = this.saveNew.bind(this)
  }

  edit (item) {
    this.props.openEdit({ item, key: USER_ROLES })
  }

  addUser () {
    this.props.addRow(USER_ROLES, this.newItemData)
  }

  saveNew (item) {
    this.props.saveNewUser(omit(item, [ 'id' ]))
      .then(data => {
        this.props.saveEdit(USER_ROLES, data, NEW_ITEM_ID)
      })
  }

  get isSubscriptionUserDefault () {
    const {
      maxSubscriptionUsers,
      subscriptionUsersCount
    } = this.props

    return maxSubscriptionUsers > subscriptionUsersCount
  }

  get newItemData () {
    return {
      id: NEW_ITEM_ID,
      jobTitle: '',
      firstName: '',
      lastName: '',
      email: '',
      phoneNumber: '',
      mobileNumber: '',
      linkedInUrl: 'www.linkedin.com/',
      isEnabled: true,
      isAdmin: false,
      agencyBranchId: null,
      agencyBranchName: null,
      isSubscriptionUser: this.isSubscriptionUserDefault
    }
  }

  get isLastAdmin () {
    const adminUsers = this.props.tableData.data.filter(user => {
      return user.isAdmin && user.isEnabled
    })
    return adminUsers.length < 2
  }

  save (item) {
    const {
      tableData,
      logout,
      openInfoModal,
      saveUser,
      saveEdit,
      getMessages
    } = this.props
    const {
      confirmToAllowTheSelectedUserAccesingGT,
      areYouSureYouWantToStopThisUserAccesingGT,
      youHaveChangedYourRoleAndNeedToBeLoggetOut,
      youWilNoLongerBeAbleToLoginGT,
      youCannotRemoveAdminPrivilagesFromThisUser,
      error
    } = getMessages()

    const editItemInitialState = tableData.data
      .find(tableItem => tableItem.id === item.id)
    const {
      userObjectId
    } = new IDTokenInfo()
    const isSelf = userObjectId === item.oAuthUserId
    const userRoleHasChanged = item.isAdmin !== editItemInitialState.isAdmin
    const isActiveHasChanged = item.isEnabled !== editItemInitialState.isEnabled

    if (editItemInitialState.isAdmin && editItemInitialState.isEnabled && this.isLastAdmin && (userRoleHasChanged || isActiveHasChanged)) {
      openInfoModal({
        description: youCannotRemoveAdminPrivilagesFromThisUser,
        title: error
      })
      return
    }

    const saveCallback = () => saveUser(item)
      .then(({ isUpdated }) => {
        isUpdated && saveEdit(USER_ROLES, item)
        if (isSelf && userRoleHasChanged) {
          logout()
        }
      })

    if (isActiveHasChanged || (isSelf && userRoleHasChanged)) {
      let description = String.empty

      if (isActiveHasChanged) {
        if ((isSelf && userRoleHasChanged)) {
          description = youWilNoLongerBeAbleToLoginGT
        } else {
          description = item.isEnabled ? confirmToAllowTheSelectedUserAccesingGT : areYouSureYouWantToStopThisUserAccesingGT
        }
      } else {
        description = youHaveChangedYourRoleAndNeedToBeLoggetOut
      }

      this.openModal(saveCallback, description)
    } else {
      saveCallback()
    }
  }

  updateIsAdminField (event) {
    let value = event && event.target && event.target.value
    // When it's select we need to cast value to bool
    if (value === 'true') value = true
    if (value === 'false') value = false

    this.props.editTableData({
      item: this.FIELDS.admin,
      key: USER_ROLES,
      value
    })
  }

  updateBranchField (event) {
    const value = event && event.target && event.target.value
    const {
      branches,
      editTableData
    } = this.props
    const branch = branches[value] || {}

    editTableData({
      item: this.FIELDS.branchId,
      key: USER_ROLES,
      value: branch.id
    })

    editTableData({
      item: this.FIELDS.branchName,
      key: USER_ROLES,
      value: branch.name
    })
  }

  updateTextField (event) {
    const {
      value,
      name
     } = event.target

    this.props.editTableData({
      item: name,
      key: USER_ROLES,
      value: value
    })
  }

  openModal (onSubmit, description) {
    const {
      yes,
      no
    } = this.props.getMessages()
    this.props.openConfirmationModal({
      okLabel: yes,
      cancelLabel: no,
      description,
      onSubmit
    })
  }

  saveAndUpdate (item, field, value) {
    this.props.saveUser({
      ...item,
      [field]: value
    })
    .then(({ isUpdated }) => {
      isUpdated && this.props.updateTableItem({
        key: USER_ROLES,
        id: item.id,
        field,
        value
      })
    })
  }

  getBranchseSelectOptions () {
    const {
      branches,
      branchesList
    } = this.props

    return branchesList.map(item => ({
      label: branches[item].name,
      value: branches[item].id
    }))
  }

  toggleUser (item, itemInEditMode = false) {
    const {
      tableData,
      getMessages,
      openInfoModal,
      editTableData
    } = this.props
    const {
      youCannotDisableThisUserAsTheyAreTheLast,
      error
    } = getMessages()

    return event => {
      let value = event && event.target && event.target.checked
      if (!itemInEditMode) {
        const isLastAdmin = tableData.data.filter(user => user.isAdmin && user.isEnabled).length < 2

        if (item.isAdmin && item.isEnabled && isLastAdmin) {
          openInfoModal({
            description: youCannotDisableThisUserAsTheyAreTheLast,
            title: error
          })
          return
        }

        const {
          confirmToAllowTheSelectedUserAccesingGT,
          areYouSureYouWantToStopThisUserAccesingGT
        } = getMessages()

        this.openModal(
          () => this.saveAndUpdate(item, this.FIELDS.enabled, value),
          value
            ? confirmToAllowTheSelectedUserAccesingGT
            : areYouSureYouWantToStopThisUserAccesingGT
        )
      } else {
        editTableData({ item: this.FIELDS.enabled, key: USER_ROLES, value })
      }
    }
  }

  toggleLicense (event) {
    const value = event && event.target && event.target.checked

    this.props.editTableData({
      item: this.FIELDS.subscription,
      key: USER_ROLES,
      value
    })
  }

  componentDidMount () {
    // We need this as the branches needs to be set before the table initiatilize
    // after the table initializes we can't update the columns config
    // safiest way is just to wait untill we got all data required
    this.props.getBranchesList()
      .then(() => {
        this.setState({ isRequiredDataFetched: true })
      })
  }

  get shouldDisplayAddButton () {
    return this.state.isRequiredDataFetched && this.props.isStandaloneAdmin
  }

  get tableConfig () {
    return tableColumns({
      // We need a method here to get the actual/current value
      isStandaloneAdmin: () => this.props.isStandaloneAdmin,
      openEdit: this.edit,
      saveEdit: this.save,
      saveNew: this.saveNew,
      updateIsAdminField: this.updateIsAdminField,
      updateBranchField: this.updateBranchField,
      toggleUserActive: this.toggleUser,
      toggleLicenseActive: this.toggleLicense,
      updateTextField: this.updateTextField,
      branches: this.getBranchseSelectOptions()
    })
  }

  render () {
    const {
      getUsers,
      getMessages,
      hasNewRecord
    } = this.props
    const {
      addUser
    } = getMessages()

    return (
      <Fragment>
        <Header />
        {this.state.isRequiredDataFetched
          ? (
            <Table
              id={USER_ROLES}
              columns={this.tableConfig}
              getMethod={getUsers}
              sortByKey='-firstName'
            />
          )
          : <BigLoader isLoading />
        }
        {this.shouldDisplayAddButton && (
          <div className='c-action-box c-action-box--simple c-action-box--small'>
            <a
              href='#'
              className={classnames(
                'c-btn t-text-3 t-upper u-border-14 u-paint-2 u-bg-14 u-bg-opacity-on-hover',
                {
                  'is-disabled': hasNewRecord
                }
              )}
              onClick={this.addUser}
            >
              <span className='c-label'>{addUser}</span>
            </a>
          </div>
        )}
      </Fragment>
    )
  }
}

UserRoles.propTypes = {
  getMessages: PropTypes.func,
  getUsers: PropTypes.func,
  openEdit: PropTypes.func,
  saveEdit: PropTypes.func,
  saveUser: PropTypes.func,
  saveNewUser: PropTypes.func,
  addRow: PropTypes.func,
  editTableData: PropTypes.func,
  updateTableItem: PropTypes.func,
  getBranchesList: PropTypes.func,
  openConfirmationModal: PropTypes.func,
  logout: PropTypes.func,
  openInfoModal: PropTypes.func,
  hasNewRecord: PropTypes.bool,
  tableData: PropTypes.object,
  branchesList: PropTypes.array,
  branches: PropTypes.object,
  isStandaloneAdmin: PropTypes.bool,
  subscriptionUsersCount: PropTypes.number,
  maxSubscriptionUsers: PropTypes.number
}

const mapStateToProps = state => {
  const table = state.tables[USER_ROLES]
  const hasNewRecord = table && table.data && table.data.find(item => item.id === NEW_ITEM_ID)
  return {
    hasNewRecord: !!hasNewRecord,
    tableData: table,
    branchesList: state.branches.list,
    branches: state.branches.items,
    isStandaloneAdmin: state.setup.isStandalone,
    subscriptionUsersCount: countSubscriptionUsers(table && table.data),
    maxSubscriptionUsers: getMaxSubscriptionCount(state.companyDetails)
  }
}

export default withLanguage(
  connect(mapStateToProps, {
    getUsers,
    openEdit,
    saveEdit,
    saveUser,
    saveNewUser,
    addRow,
    editTableData,
    updateTableItem,
    getBranchesList,
    openConfirmationModal,
    logout,
    openInfoModal
  })(UserRoles)
)
