/*  global FormData */
import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Cropper from 'react-cropper'
import Dropzone from 'react-dropzone'
import { toggleFetchLoader } from 'platform/app/AppActions'
import {
  getImageFromCropper,
  getUriFromFile,
  getImageFromUrlWithCredentials
} from 'utils/images'
import {
  updateAvatar,
  updateAvatarPreview
} from 'platform/candidate/details/Actions'
import 'cropperjs/dist/cropper.css'
import withLanguage from 'common/withLanguage'

class AddAvatar extends React.Component {
  constructor (props) {
    super(props)

    this.cropper = null

    this.crop = this.crop.bind(this)
    this.uploadFile = this.uploadFile.bind(this)
    this.hideLoader = this.hideLoader.bind(this)
  }

  prepareToSubmit (dataUrl) {
    const fileData = new FormData()
    const file = getImageFromCropper(this.cropper)
    fileData.append('File', file, `${file.name}.png`)
    return fileData
  }

  hideLoader () {
    this.props.toggleFetchLoader(false)
  }

  uploadFile (acceptedFiles) {
    this.props.toggleFetchLoader(true)
    getUriFromFile(acceptedFiles[0])
      .then(image => {
        this.props.updateAvatarPreview(image)
        this.props.toggleFetchLoader(false)
      })
  }

  crop () {
    const croppedFile = this.cropper.getCroppedCanvas().toDataURL()
    const file = this.prepareToSubmit(croppedFile)

    this.props.updateAvatar(file)
  }

  componentDidMount () {
    if (this.props.avatar && !this.props.avatarPreview) {
      this.props.toggleFetchLoader(true)
      getImageFromUrlWithCredentials(this.props.avatar)
        .then(response => {
          this.props.updateAvatarPreview(response)
          this.props.toggleFetchLoader(false)
        })
        .catch(() => {
          this.props.toggleFetchLoader(false)
        })
    }
  }

  render () {
    const {
      avatarPreview,
      onCancel,
      getMessages
    } = this.props
    const {
      uploadNewImage,
      save,
      uploadImageFirst,
      cancel
    } = getMessages()

    return (
      <div className='c-crop'>
        {avatarPreview
          ? (
            <Cropper
              ref={ref => { this.cropper = ref }}
              src={avatarPreview}
              style={{ height: 300, width: '100%' }}
              ascpectRatio={1}
              dragMode='move'
              movable={false}
              viewMode={2}
              autoCropArea={1}
              guides={false}
              aspectRatio={1}
              toggleDragModeOnDblclick={false}
              responsive
              minContainerWidth={100}
            />
          )
          : (
            <Dropzone
              onDrop={this.uploadFile}
              multiple={false}
              accept='image/jpeg,image/jpg,image/gif,image/png'
              className='c-crop__empty'
            >
              {uploadImageFirst}
            </Dropzone>
          )
        }
        <div className='c-modal__action c-modal__action--three-in-row'>
          <Dropzone
            onDrop={this.uploadFile}
            multiple={false}
            accept='image/jpeg,image/jpg,image/gif,image/png'
            className='c-crop__upload'
          >
            <button
              className='c-btn c-btn--modal c-btn--confirm'
            >
              {uploadNewImage}
            </button>
          </Dropzone>
          <button
            onClick={this.crop}
            className='c-btn c-btn--modal c-btn--confirm'
          >
            {save}
          </button>
          <button
            onClick={onCancel}
            className='c-btn c-btn--modal c-btn--cancel'
          >
            {cancel}
          </button>
        </div>
      </div>
    )
  }
}

AddAvatar.propTypes = {
  updateAvatarPreview: PropTypes.func,
  updateAvatar: PropTypes.func,
  onCancel: PropTypes.func,
  toggleFetchLoader: PropTypes.func,
  avatar: PropTypes.string,
  avatarPreview: PropTypes.string,
  getMessages: PropTypes.func
}

const mapStateToProps = state => ({
  avatar: state.candidate.userDetails.photoUrl,
  avatarPreview: state.candidate.avatarPreview
})

export default connect(mapStateToProps, {
  updateAvatar,
  updateAvatarPreview,
  toggleFetchLoader
})(withLanguage(AddAvatar))
