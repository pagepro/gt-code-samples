import React from 'react'
import {
  connect
} from 'react-redux'
import {
  Route,
  Redirect
} from 'react-router-dom'
import { setWorkWithUsData } from 'platform/workWithUs/Actions'
import {
  logOut,
  validateAuthCookie,
  AUTH_COOKIE_NAME
} from 'platform/signIn/SignInActions'
import PropTypes from 'prop-types'
import {
  remove as removeCookie
} from 'js-cookie'
import config from 'platform/setup/config'
import { extractParamsFromQuery } from 'utils/browser'
import { tryGetWorkWithUsData } from 'platform/workWithUs/Utils'
import BigLoader from 'common/BigLoader'
import { changeLanguage } from 'platform/app/AppActions'

class PreLoginRoute extends Route {
  constructor (...args) {
    super(...args)

    Object.assign(this.state, {
      isCookieChecked: false,
      isCookieValid: false
    })
  }

  componentDidMount () {
    validateAuthCookie()
      .then(() => {
        this.setState({
          isCookieValid: true
        })
      }, () => {
        removeCookie(AUTH_COOKIE_NAME)
        this.setState({
          isCookieValid: false
        })
      })
      .then(() => {
        this.setState({
          isCookieChecked: true
        }, () => {
          const {
            location: {
              search
            },
            setWorkWithUsData,
            history,
            logOut,
            changeLanguage
          } = this.props
          const {
            agency
          } = extractParamsFromQuery(search)
          const workWithUsData = tryGetWorkWithUsData(search)

          if (workWithUsData) {
            setWorkWithUsData(workWithUsData)
          }

          if (agency && this.state.isCookieValid) {
            logOut(history, `?agency=${agency}`)
          }

          changeLanguage(config.defaultLanguage)
        })
      })
  }

  render () {
    const {
      isCookieChecked,
      isCookieValid
    } = this.state
    const {
      notLoggedInRoute,
      component,
      location: {
        search
      },
      path,
      exact
    } = this.props
    const {
      returnUrl,
      agency
    } = extractParamsFromQuery(search)

    if (!isCookieChecked || (agency && isCookieValid)) return <BigLoader isVisible />

    return component
      ? (isCookieValid
        ? (
          <Redirect
            to={returnUrl || config.routes.candidate}
          />
        ) : (
          <Route
            path={path}
            exact={exact}
            component={component}
          />
        )
      ) : (
        <Redirect
          to={isCookieValid ? config.routes.candidate : notLoggedInRoute}
        />
      )
  }
}

PreLoginRoute.propTypes = {
  notLoggedInRoute: PropTypes.string,
  logOut: PropTypes.func,
  history: PropTypes.object
}

export default connect(null, {
  setWorkWithUsData,
  logOut,
  changeLanguage
})(PreLoginRoute)
