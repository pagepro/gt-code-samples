import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import config from 'platform/setup/config'
import withLanguage from 'common/withLanguage'
import {
  premiumAdBgAlt
} from 'platform/setup/images'
import GTSygnet from 'platform/icons/SygnetGT'
import { hasPremiumFeature } from 'utils/helpers'

class PremiumAdBlock extends React.Component {
  get descriptions () {
    const {
      appearAtTheTopOfRecruiterSearchResults,
      expectMoreIncomingConnectionRequests,
      accessPremiumContent,
      continueToProtectYourPrivacy
    } = this.props.getMessages()

    return [
      appearAtTheTopOfRecruiterSearchResults,
      expectMoreIncomingConnectionRequests,
      accessPremiumContent,
      continueToProtectYourPrivacy
    ]
  }

  renderDescription (text, index) {
    return (
      <li
        key={index}
        className='c-dotted-list__item'
      >
        <p className='t-text-1 t-weight-light'>
          {text}
        </p>
      </li>
    )
  }

  render () {
    const {
      goPremium,
      upgradeNowToIncreaseYourChances
    } = this.props.getMessages()

    if (!hasPremiumFeature()) {
      return null
    }

    return (
      <Link
        to={config.routes.premium}
        className='c-premium-ad u-paint-2'
      >
        <div
          className='c-premium-ad__bg'
          style={{
            backgroundImage: `url(${premiumAdBgAlt})`
          }}
        />
        <div className='c-premium-ad__content'>
          <div className='c-premium-ad__heading'>
            <GTSygnet />
            <h2 className='t-text-8 t-weight-bold t-center u-paint-31'>{goPremium}</h2>
          </div>
          <div className='c-premium-ad__title'>
            <h2 className='t-text-11 t-weight-light t-center'>{upgradeNowToIncreaseYourChances}</h2>
          </div>
          <div className='c-premium-ad__desc'>
            <ul className='c-dotted-list'>
              {this.descriptions.map(this.renderDescription)}
            </ul>
          </div>
          <div className='c-premium-ad__action'>
            <span className='c-btn c-btn--premium u-bg-31 u-paint-2'>
              <span className='c-label t-upper t-heading-9 t-weight-medium'>
                {goPremium}
              </span>
            </span>
          </div>
        </div>
      </Link>
    )
  }
}

PremiumAdBlock.propTypes = {
  getMessages: PropTypes.func
}

export default withLanguage(PremiumAdBlock)
