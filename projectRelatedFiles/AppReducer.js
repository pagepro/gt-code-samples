import * as actions from 'platform/setup/actionTypes'
import config from 'platform/setup/config'

export const defaultState = {
  loaders: {
    fetch: false,
    pageLoad: false,
    transparent: true
  },
  language: config.defaultLanguage,
  notifications: [],
  isMobile: false,
  languageList: []
}

export default (state = defaultState, action) => {
  const { type, payload } = action
  switch (type) {
    case actions.TOGGLE_PAGE_LOADER:
      return {
        ...state,
        loaders: {
          ...state.loaders,
          pageLoad: payload.value,
          transparent: payload.transparent
        }
      }
    case actions.TOGGLE_FETCH_LOADER:
      return {
        ...state,
        loaders: {
          ...state.loaders,
          fetch: payload.value,
          transparent: payload.transparent
        }
      }
    case actions.CHANGE_LANGUAGE:
      return {
        ...state,
        language: payload
      }
    case actions.ADD_NOTIFICATION:
      return {
        ...state,
        notifications: [...state.notifications, payload]
      }
    case actions.REMOVE_NOTIFICATION:
      return {
        ...state,
        notifications: state.notifications.filter(notification => notification.id !== payload)
      }
    case actions.REMOVE_ALL_NOTIFICATIONS:
      return {
        ...state,
        notifications: state.notifications.filter(item => item.closeOnLeave !== true)
      }
    case actions.UPDATE_VIEWPORT:
      return {
        ...state,
        isMobile: action.payload
      }
    case actions.GET_REQUEST_LANGUAGES:
      return {
        ...state,
        languageList: payload
      }
    default:
      return state
  }
}
