import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import classNames from 'classnames'
import {
  withRouter
} from 'react-router'
import {
  NOT_STARTED,
  PROGRESS_FAILED
} from 'platform/setup/boardingStatuses'
import {
  updateUserBoarding,
  uploadCVFile,
  startImporting,
  clearProgressData,
  getBoardingProgress,
  getCVFile
} from 'platform/boarding/BoardingActions'
import { removeDocument } from 'platform/candidate/documents/Actions'
import {
  toggleNotLoaded
} from 'platform/candidate/Actions'
import FileInput from 'platform/boarding/FileInput'
import DocumentsList from 'platform/candidate/documents/List'
import { pushErrorNotification } from 'platform/app/AppActions'
import config from 'platform/setup/config'
import withLanguage from 'common/withLanguage'
import {
  getFileExtension,
  convertMBtoBytes
} from 'utils/helpers'
import { ALLOWED_FILES } from './boardingTypes'

class ConnectDocument extends Component {
  constructor () {
    super()

    this.state = {
      file: null,
      isChecked: false
    }

    this.onSubmit = this.onSubmit.bind(this)
    this.finalCallback = this.finalCallback.bind(this)
    this.updateFile = this.updateFile.bind(this)
    this.deleteAction = this.deleteAction.bind(this)
    this.onError = this.onError.bind(this)
    this.goBack = this.goBack.bind(this)
    this.checkStatus = this.checkStatus.bind(this)
  }

  componentDidMount () {
    this.checkStatus()
  }

  get fileSize () {
    return 5
  }

  get maxSize () {
    return convertMBtoBytes(this.fileSize)
  }

  checkStatus () {
    const {
      getBoardingProgress,
      clearProgressData,
      getCVFile
    } = this.props

    // This is the flow:
    // Check if user has already processing any file [getBoardingProgress]
    // (NO) - 404 response -> clear progress data, fetch the docs and stay on page
    // (YES) -> go to processing page
    getBoardingProgress()
      .then(data => {
        if (data.error || data.status === PROGRESS_FAILED) {
          clearProgressData()
          getCVFile()
            .then(files => {
              this.setState({ file: files[0] })
            })
        } else {
          this.finalCallback()
        }
      })
  }

  deleteAction () {
    this.props.removeDocument(this.state.file.id)
      .then(() => {
        this.setState({ file: null })
      })
  }

  updateFile (files) {
    this.props.uploadCVFile(files[0])
      .then(file => {
        this.setState({ file })
      })
  }

  goBack (event) {
    event.preventDefault()

    this.props.updateUserBoarding({
      onBoardingStatus: NOT_STARTED
    }, () => {
      this.props.history.push(config.routes.connectNotStarted)
    })
  }

  getErrorMessage (rejectedFile) {
    const {
      fileTypeNotSupported,
      fileSizeMustBeNoLargerThanXMb,
      somethingWentWrong
    } = this.props.getMessages()

    if (
      !(
        ALLOWED_FILES.includes(rejectedFile.type) ||
        ALLOWED_FILES.includes(getFileExtension(rejectedFile.name))
      )
    ) {
      return fileTypeNotSupported
    }

    if (rejectedFile.size > this.maxSize) {
      return fileSizeMustBeNoLargerThanXMb.format(this.fileSize)
    }

    return somethingWentWrong
  }

  onError (files) {
    const {
      pushErrorNotification
    } = this.props

    const [ rejectedFile ] = files

    if (rejectedFile) {
      pushErrorNotification(this.getErrorMessage(rejectedFile))
    }
  }

  finalCallback () {
    const {
      toggleNotLoaded,
      history
    } = this.props

    toggleNotLoaded(true)
    history.push(config.routes.connectProgress)
  }

  onSubmit (event) {
    event.preventDefault()

    this.props.startImporting()
      .then(this.finalCallback)
  }

  render () {
    const {
      getMessages
    } = this.props
    const {
      file
    } = this.state
    const {
      next,
      backToChooseAnotherOption,
      uploadYourResumeCv,
      fileSizeLimitXMBPerDocument,
      supportedFileFormatsCV
    } = getMessages()

    return (
      <form
        onSubmit={this.onSubmit}
        className='f-form f-form--boarding'
      >
        <fieldset className='f-form__fieldset'>
          <div className='f-form__body'>
            <div className='f-form__row'>
              <DocumentsList
                deleteAction={this.deleteAction}
                documents={file ? [ file ] : []}
                isOnBoarding
              />
              <FileInput
                disabledClassName='is-disabled'
                className='f-form__overlay-wrapper'
                value={file}
                multiple={false}
                onChange={this.updateFile}
                onError={this.onError}
                disabled={!!file}
                accept={ALLOWED_FILES}
                maxSize={this.maxSize}
              >
                <button
                  className='c-btn c-btn--drop t-heading-2 s-medium t-upper ui-paint-grey-2'
                  type='button'
                >
                  {uploadYourResumeCv}
                </button>
              </FileInput>
              <div className='f-form__file-info'>
                <p className='t-style-error s-regular ui-paint-grey-2'>
                  {supportedFileFormatsCV}
                </p>
                <p className='t-style-error s-regular ui-paint-grey-2'>
                  {fileSizeLimitXMBPerDocument.format(5)}
                </p>
              </div>
            </div>
          </div>
        </fieldset>
        <div className='f-form__action'>
          <button
            type='submit'
            className={classNames(
              'f-form__btn ui-bg-blue-2', {
                'f-form__btn--disabled': !this.state.file
              })
            }
            disabled={!this.state.file}
          >
            {next}
          </button>
          <button
            type='button'
            onClick={this.goBack}
            className='f-form__btn f-form__btn-link ui-bg-white u-paint-9'
          >
            {backToChooseAnotherOption}
          </button>
        </div>
      </form>
    )
  }
}

ConnectDocument.propTypes = {
  handleSubmit: PropTypes.func,
  searchSkills: PropTypes.func,
  searchCountries: PropTypes.func,
  searchLanguagesWithProficiency: PropTypes.func,
  updateUserBoarding: PropTypes.func,
  toggleNotLoaded: PropTypes.func,
  removeDocument: PropTypes.func,
  startImporting: PropTypes.func,
  uploadCVFile: PropTypes.func,
  getCVFile: PropTypes.func,
  history: PropTypes.object,
  isNewsletterSelected: PropTypes.bool,
  getMessages: PropTypes.func,
  pushErrorNotification: PropTypes.func,
  clearProgressData: PropTypes.func,
  getBoardingProgress: PropTypes.func
}

const mapStateToProps = state => ({
  isNewsletterSelected: state.boarding.newsletter
})

export default withRouter(
  connect(mapStateToProps, {
    pushErrorNotification,
    toggleNotLoaded,
    updateUserBoarding,
    uploadCVFile,
    removeDocument,
    getCVFile,
    startImporting,
    getBoardingProgress,
    clearProgressData
  })(withLanguage(ConnectDocument))
)
