import * as events from 'common/appInsightEvents'
import { isDev } from 'utils/helpers'

class AppInsights {
  constructor () {
    this.globalInstance = window.appInsights

    this.events = {
      pages: {
        lr: this.trackPage(events.LR_PAGE),
        cr: this.trackPage(events.CR_PAGE),
        confirmation: this.trackPage(events.CONFIRMATION_PAGE),
        signup: this.trackPage(events.SIGNUP_PAGE),
        signin: this.trackPage(events.SIGNIN_PAGE)
      },
      lr: {
        accordion: {
          whyWeCollect: this.trackEvent(events.LIN_WHY_DO_WE_COLLECT_TOGGLED),
          whatInformation: this.trackEvent(events.LIN_WHAT_INFORMATION_TOGGLED),
          whatWeDo: this.trackEvent(events.LIN_WHAT_WILL_WE_DO_WITH_YOUR_DATA_TOGGLED),
          whoWillShare: this.trackEvent(events.LIN_WHO_WILL_WE_SHARE_YOUR_DATA_TOGGLED),
          howLong: this.trackEvent(events.LIN_HOW_LONG_WILL_WE_HOLD_YOUR_DATA_TOGGLED),
          yourRights: this.trackEvent(events.LIN_YOUR_RIGHTS_TOGGLED)
        },
        custom: {
          languageChange: this.trackEvent(events.LIN_LANGUAGE_CHANGE),
          privacy: this.trackEvent(events.LIN_PRIVACY_CLICKED),
          terms: this.trackEvent(events.LIN_TANDC_CLICKED),
          getStarted: this.trackEvent(events.LIN_LETS_GET_STARTED_CLICKED)
        },
        tabs: {
          accept: this.trackEvent(events.LIN_ACCEPT_TAB_SELECTED),
          reject: this.trackEvent(events.LIN_REJECT_TAB_SELECTED),
          notnow: this.trackEvent(events.LIN_NOT_RIGHT_NOW_TAB_SELECTED)
        },
        decision: {
          accept: this.trackEvent(events.LIN_ACCEPTED_CLICKED),
          reject: this.trackEvent(events.LIN_REJECTED_CLICKED),
          notnow3: this.trackEvent(events.LIN_NOT_RIGHT_NOW_3_CLICKED),
          notnow6: this.trackEvent(events.LIN_NOT_RIGHT_NOW_6_CLICKED),
          notnow12: this.trackEvent(events.LIN_NOT_RIGHT_NOW_12_CLICKED)
        }
      },
      cr: {
        accordion: {
          whyWeCollect: this.trackEvent(events.CONSENT_WHY_DO_WE_COLLECT_TOGGLED),
          whatInformation: this.trackEvent(events.CONSENT_WHAT_INFORMATION_TOGGLED),
          whatWeDo: this.trackEvent(events.CONSENT_WHAT_WILL_WE_DO_WITH_YOUR_DATA_TOGGLED),
          whoWillShare: this.trackEvent(events.CONSENT_WHO_WILL_WE_SHARE_YOUR_DATA_TOGGLED),
          howLong: this.trackEvent(events.CONSENT_HOW_LONG_WILL_WE_HOLD_YOUR_DATA_TOGGLED),
          yourRights: this.trackEvent(events.CONSENT_YOUR_RIGHTS_TOGGLED)
        },
        custom: {
          languageChange: this.trackEvent(events.CONSENT_LANGUAGE_CHANGE)
        },
        tabs: {
          accept: this.trackEvent(events.CONSENT_ACCEPT_TAB_SELECTED),
          reject: this.trackEvent(events.CONSENT_REJECT_TAB_SELECTED),
          notnow: this.trackEvent(events.CONSENT_NOT_RIGHT_NOW_TAB_SELECTED)
        },
        decision: {
          accept: this.trackEvent(events.CONSENT_ACCEPTED_CLICKED),
          reject: this.trackEvent(events.CONSENT_REJECTED_CLICKED),
          notnow3: this.trackEvent(events.CONSENT_NOT_RIGHT_NOW_3_CLICKED),
          notnow6: this.trackEvent(events.CONSENT_NOT_RIGHT_NOW_6_CLICKED),
          notnow12: this.trackEvent(events.CONSENT_NOT_RIGHT_NOW_12_CLICKED)
        }
      },
      confirmation: {
        languageChange: this.trackEvent(events.POST_CONSENT_LANGUAGE_CHANGE),
        login: this.trackEvent(events.POST_CONSENT_LOGIN),
        signup: this.trackEvent(events.POST_CONSENT_SIGNUPPASSWORD_CLICKED),
        startFreeAccount: this.trackEvent(events.POST_CONSENT_START_FREE_ACCOUNT_CLICKED)
      },
      signInPage: {
        gtLogoClicked: this.trackEvent(events.SIGNIN_GT_LOGO_CLICKED),
        gtPrivacyPolicyClicked: this.trackEvent(events.SIGNIN_GT_GT_PRIVACYPOLICY_CLICKED),
        gtTandcClicked: this.trackEvent(events.SIGNIN_GT_TANDC_CLICKED),
        loginWithLinkedInClicked: this.trackEventImmediately(events.SIGNIN_LOGINWITHLINKEDIN_CLICKED),
        emailClicked: this.trackEvent(events.SIGNIN_EMAIL_CLICKED),
        passwordClicked: this.trackEvent(events.SIGNIN_PASSWORD_CLICKED),
        signInButtonClicked: this.trackEvent(events.SIGNIN_SIGNINBUTTON_CLICKED),
        forgotPasswordClicked: this.trackEvent(events.SIGNIN_FORGOTPASSWORD_CLICKED),
        signUpButtonClicked: this.trackEvent(events.SIGNIN_SIGNUPBUTTON_CLICKED)
      },
      signUpPage: {
        gtLogoClicked: this.trackEvent(events.SIGNUP_GT_LOGO_CLICKED),
        gtPrivacyPolicyClicked: this.trackEvent(events.SIGNUP_GT_PRIVACYPOLICY_CLICKED),
        gtTandcClicked: this.trackEvent(events.SIGNUP_GT_TANDC_CLICKED),
        joinWithLinkedInClicked: this.trackEventImmediately(events.SIGNUP_JOINWITHLINKEDIN_CLICKED),
        firstnameClicked: this.trackEvent(events.SIGNUP_FIRSTNAME),
        surnameClicked: this.trackEvent(events.SIGNUP_SURNAME),
        emailClicked: this.trackEvent(events.SIGNUP_EMAILADDRESS),
        passwordClicked: this.trackEvent(events.SIGNUP_PASSWORD),
        captchaClicked: this.trackEvent(events.SIGNUP_CAPTCHA),
        joinNowClicked: this.trackEvent(events.SIGNUP_JOINNOWBUTTON),
        loginClicked: this.trackEvent(events.SIGNUP_LOGINBUTTON)
      },
      resetPassword: {
        captchaClicked: this.trackEvent(events.RESET_PASSWORD_CAPTCHA)
      },
      common: {
        email: this.trackEvent(events.REQUEST_SIGNUPEMAIL_CLICKED),
        password: this.trackEvent(events.REQUEST_SIGNUPPASSWORD_CLICKED)
      },
      search: {
        advancedSearchRun: this.trackEvent(events.ADVANCED_SEARCH_RUN),
        standardSearchRun: this.trackEvent(events.STANDARD_SEARCH_RUN),
        switchSearch: this.trackEvent(events.SEARCH_TYPE_SWITCH)
      }
    }
  }

  trackPage (name) {
    return () => {
      if (isDev()) {
        console.log('track page', name)
      }
      this.globalInstance && this.globalInstance.trackPageView(name)
    }
  }

  trackEvent (name) {
    return props => {
      if (isDev()) {
        console.log('track event', props, name)
      }
      this.globalInstance && this.globalInstance.trackEvent(name, props)
    }
  }

  trackEventImmediately (name) {
    return () => new Promise(resolve => {
      const prevMaxBatchinterval = this.globalInstance.config.maxBatchInterval
      this.globalInstance.config.maxBatchInterval = 1
      this.trackEvent(name)()
      setTimeout(() => {
        this.globalInstance.config.maxBatchInterval = prevMaxBatchinterval
        resolve()
      }, 100)
    })
  }
}

const appInsightsInstance = new AppInsights()

export default appInsightsInstance
