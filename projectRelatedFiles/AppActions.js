import * as actions from 'platform/setup/actionTypes'
import config from 'platform/setup/config'
import axios from 'axios'
import { checkResponse } from 'utils/helpers'
import {
  skipInterceptorObject,
  skipAuthorizationInterceptorObject
} from 'platform/setup/interceptor'

const {
  candidateBase,
  signIn: signInEndpoint
} = config.api

const SUCCESS_NOTIFICATION_TYPE = 'success'
const ERROR_NOTIFICATION_TYPE = 'danger'

const togglePageLoaderAction = (value = true, transparent = true) => ({
  type: actions.TOGGLE_PAGE_LOADER,
  payload: {
    value,
    transparent
  }
})

const toggleFetchLoaderAction = (value = true, transparent = true) => ({
  type: actions.TOGGLE_FETCH_LOADER,
  payload: {
    value,
    transparent
  }
})

const changeLanguageAction = language => ({
  type: actions.CHANGE_LANGUAGE,
  payload: language
})

const addNotification = data => ({
  type: actions.ADD_NOTIFICATION,
  payload: data
})

const removeNotification = id => ({
  type: actions.REMOVE_NOTIFICATION,
  payload: id
})

const removeAllNotifications = () => ({
  type: actions.REMOVE_ALL_NOTIFICATIONS
})

export const withLoader = (callback, transparent = true) => dispatch => {
  dispatch(toggleFetchLoader(true, transparent))
  return new Promise((resolve, reject) => {
    try {
      const callbackResult = callback(resolve)
      if (callbackResult instanceof Promise) {
        callbackResult.catch((...args) => {
          dispatch(toggleFetchLoader(false))
          return reject.apply(undefined, args)
        })
      }
    } catch (ex) {
      dispatch(toggleFetchLoader(false))
      return reject(ex)
    }
  })
  .then(callbackValue => {
    dispatch(toggleFetchLoader(false))
    return callbackValue
  })
}

export const togglePageLoader = value => dispatch => (
  dispatch(togglePageLoaderAction(value))
)

export const toggleFetchLoader = value => dispatch => (
  dispatch(toggleFetchLoaderAction(value))
)

export const pushNotification = data => dispatch => {
  const { type, headline, message, id, closeOnLeave } = data
  const newNotification = {
    id: id || (new Date().getTime()),
    type,
    headline,
    message,
    closeOnLeave
  }
  dispatch(addNotification(newNotification))
}

export const pushErrorNotification = (message, closeOnLeave = true) => dispatch => {
  dispatch(pushNotification({
    type: ERROR_NOTIFICATION_TYPE,
    message,
    closeOnLeave
  }))
}

export const pushSuccessNotification = (message, closeOnLeave = true) => dispatch => {
  dispatch(pushNotification({
    type: SUCCESS_NOTIFICATION_TYPE,
    message,
    closeOnLeave
  }))
}

export const popNotification = data => dispatch => {
  const { id } = data
  dispatch(removeNotification(id))
}

export const popAllNotifications = () => dispatch => {
  dispatch(removeAllNotifications())
}

export const changeLanguage = language => dispatch => {
  dispatch(changeLanguageAction(language))
  // TODO post language change to the server and get new content
}

const updateViewportAction = isMobile => ({
  type: actions.UPDATE_VIEWPORT,
  payload: isMobile
})

export const updateViewPort = () => {
  return (dispatch, getState) => {
    const isMobile = window.innerWidth <= 680
    const currentState = getState().setup.isMobile
    if (isMobile !== currentState) {
      dispatch(updateViewportAction(isMobile))
    }
  }
}

const checkUserPasswordAction = () => ({
  type: actions.CHECK_USER_PASSWORD
})

export const confirmPasswordOnServer = data => {
  const url = `${candidateBase}${signInEndpoint}`
  return axios.post(url, data, {
    ...skipInterceptorObject,
    ...skipAuthorizationInterceptorObject
  })
    .then(checkResponse)
}

export const confirmPassword = data => dispatch => {
  dispatch(toggleFetchLoader(true))
  return confirmPasswordOnServer(data)
    .then(response => {
      dispatch(checkUserPasswordAction)
      dispatch(toggleFetchLoader(false))
      return true
    })
    .catch(() => {
      dispatch(toggleFetchLoader(false))
      return false
    })
}
