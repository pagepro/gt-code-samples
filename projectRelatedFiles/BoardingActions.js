import * as actions from 'platform/setup/actionTypes'
import * as statuses from 'platform/setup/boardingStatuses'
import axios from 'axios'
import config from 'platform/setup/config'
import {
  withLoader
} from 'platform/app/AppActions'
import {
  getErrorDataFromResponse,
  checkResponse,
  fixFileName
} from 'utils/helpers'
import {
  addPositionAction,
  formatPositionValues
} from 'platform/candidate/employment/Actions'
import {
  getDocumentsFromServer
} from 'platform/candidate/documents/Actions'
import { skipInterceptorObject } from 'platform/setup/interceptor'

const {
  candidateBase,
  onBoarding,
  onBoardingCV,
  processCV,
  candidatesSocialImport,
  candidatesEmploymentHistoriesOnBoarding,
  addMultipleSkills,
  addMultipleCountries,
  addMultipleLanguages
} = config.api

export const getUserBoardingStatusAction = value => ({
  type: actions.BOARD_USER,
  payload: value
})

export const signUserIn = value => ({
  type: actions.LOGIN_USER,
  payload: value
})

const updateBoardingProgress = data => ({
  type: actions.UPDATE_BOARDING_PROGRESS,
  payload: data
})

const addPositionOnServer = data => {
  const url = `${candidateBase}${candidatesEmploymentHistoriesOnBoarding}`
  return axios.post(url, data)
    .then(checkResponse)
}

export const addPosition = data => dispatch => {
  const fixedData = {
    ...formatPositionValues(data, true),
    isHeadline: true
  }
  return dispatch(withLoader(resolve => {
    return addPositionOnServer(fixedData)
      .then(newPosition => {
        resolve({ isUpdatedSuccessfully: true })
        dispatch(addPositionAction(newPosition))
      })
  }))
}

export const getUserBoardingStatusFromServer = () => {
  const url = `${candidateBase}${onBoarding}`
  return axios.get(url)
    .then(response => response.data)
}

const updateBoardingOnServer = data => {
  const url = `${candidateBase}${onBoarding}`
  const {
    hasOptedInForMarketingEmails,
    onBoardingStatus
  } = data
  return axios.put(url, { hasOptedInForMarketingEmails, onBoardingStatus })
    .then(response => response.data)
}

export const updateUserBoarding = (data, callback = Function.empty) => dispatch => {
  return dispatch(withLoader(resolve => {
    return updateBoardingOnServer(data)
      .then(boardingStatus => {
        dispatch(getUserBoardingStatusAction(boardingStatus))
        callback()
        if (data.onBoardingStatus !== statuses.IMPORTING_LINKEDIN) {
          resolve()
        }
      })
  }))
}

const updateUserDetailsOnServer = data => {
  const url = `${candidateBase}${candidatesSocialImport}`
  return axios.post(url, data)
}

const getBoardingProgressFromServer = () => {
  const url = `${candidateBase}${candidatesSocialImport}`
  return axios.get(url, skipInterceptorObject).then(response => response.data)
}

export const updateUserDetails = data => (dispatch, getState) => {
  const { user, source, error } = data
  const { COMPLETE } = statuses
  if (error) {
    console.warn(error)
    // TODO: Fix linter error
    // Proposed solution return Promise.reject(new Error(error))
    // eslint-disable-next-line
    return Promise.reject()
  } else {
    const { newsletter } = getState().boarding
    return updateUserDetailsOnServer({
      source,
      importData: JSON.stringify({ user, error })
    })
      .then(response => {
        return dispatch(updateUserBoarding({
          hasOptedInForMarketingEmails: newsletter,
          onBoardingStatus: COMPLETE
        }))
      })
      .catch(() => { })
  }
}

export const getBoardingProgress = () => dispatch => {
  return getBoardingProgressFromServer()
    .then(data => {
      dispatch(updateBoardingProgress(data))
      return data
    })
    .catch(error => {
      const errorMessageWithCode = getErrorDataFromResponse(error).message
      const errorMessage = {
        error: `${error.message}. ${errorMessageWithCode}`
      }
      dispatch(updateBoardingProgress(errorMessage))
      return errorMessage
    })
}

export const authorizeUser = () => dispatch => {
  return getUserBoardingStatusFromServer()
    .then(response => {
      dispatch(signUserIn(true))
      dispatch(getUserBoardingStatusAction(response))
      return response
    })
    .catch(() => {
      dispatch(signUserIn(false))
      return false
    })
}

export const unauthorizeUser = () => dispatch => {
  dispatch(getUserBoardingStatusAction({}))
}

const saveSkillsOnServer = skills => {
  return axios.put(`${candidateBase}${addMultipleSkills}`, {
    skills
  })
}

const saveCountriesOnServer = countries => {
  return axios.put(`${candidateBase}${addMultipleCountries}`, {
    countries
  })
}

const saveLanguagesOnServer = languages => {
  return axios.put(`${candidateBase}${addMultipleLanguages}`, {
    languages
  })
}

const uploadCVToServer = file => {
  const data = new window.FormData()
  const newFileName = fixFileName(file.name)

  data.append('File', file, newFileName)

  const url = `${candidateBase}${onBoardingCV}`
  return axios.put(url, data)
    .then(checkResponse)
}

const createPromises = data => {
  const {
    languages,
    skills,
    countries,
    file
  } = data

  const promises = []

  if (languages && languages.length) {
    promises.push(saveLanguagesOnServer(languages))
  }
  if (skills && skills.length) {
    promises.push(saveSkillsOnServer(skills))
  }
  if (countries && countries.length) {
    promises.push(saveCountriesOnServer(countries))
  }
  if (file) {
    promises.push(uploadCVToServer(file))
  }

  return promises
}

export const saveSkills = data => dispatch => {
  return dispatch(withLoader(resolve => {
    const promises = createPromises(data)

    return Promise.all(promises)
      .then(() => resolve(true))
  }))
}

export const uploadCVFile = file => dispatch => {
  return dispatch(withLoader(resolve => {
    return uploadCVToServer(file)
      .then(resolve)
  }))
}

export const startImporting = () => dispatch => {
  return dispatch(withLoader(resolve => {
    return axios.post(`${candidateBase}${processCV}`)
      .then(resolve)
  }))
}

export const getCVFile = () => dispatch => {
  return dispatch(withLoader(resolve => {
    return getDocumentsFromServer()
      .then(data => {
        resolve(data)
      })
  }))
}

const cancelImportingOnServer = () => {
  return axios.delete(`${candidateBase}${processCV}`)
}

export const cancelImportingCV = () => dispatch => {
  return dispatch(withLoader(resolve => {
    return cancelImportingOnServer()
      .then(resolve)
  }))
}

export const clearProgressData = () => dispatch => {
  dispatch(updateBoardingProgress({}))
}
