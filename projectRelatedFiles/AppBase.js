import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { subscribe } from 'common/reduxRouter'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { AlertList } from 'react-bs-notifier'
import {
  popNotification,
  popAllNotifications,
  updateViewPort
} from 'platform/app/AppActions'
import { scrollRoutes } from 'platform/setup/config'
import Modal from 'platform/modals/Modal'
import BigLoader from 'common/BigLoader'

const NOTIFICATION_TIMEOUT = 10000

class App extends Component {
  constructor (props) {
    super(props)

    this.updateWindowDimensions = this.updateWindowDimensions.bind(this)
    this.addRouteListener = this.addRouteListener.bind(this)
    this.scrollTopOnRouteChange = this.scrollTopOnRouteChange.bind(this)
  }

  updateWindowDimensions () {
    this.props.updateViewPort()
  }

  scrollTopOnRouteChange (location) {
    const routes = this.props.isMobile ? scrollRoutes.mobile : scrollRoutes.desktop
    if (routes[location.pathname]) {
      window.scroll(0, 0)
    }
  }

  addRouteListener () {
    const {
      history,
      popAllNotifications
    } = this.props

    history.listen(location => {
      popAllNotifications(location)
      this.scrollTopOnRouteChange(location)
    })
  }

  componentDidMount () {
    const { history } = this.props
    // add listener to switch views between mobile and desktop
    this.updateWindowDimensions()
    window.addEventListener('resize', this.updateWindowDimensions)

    this.addRouteListener()
    subscribe(history)
  }

  componentWillUnmount () {
    window.removeEventListener('resize', this.updateWindowDimensions)
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.language !== this.props.language) {
      this.forceUpdate()
    }
  }

  getChildContext () {
    return { language: this.props.language }
  }

  render () {
    const {
      loaders: {
        pageLoad,
        fetch,
        transparent
      },
      children,
      notifications,
      popNotification
    } = this.props

    return (
      <div className='inner'>
        <BigLoader
          isLoading={pageLoad || fetch}
          transparent={transparent}
        />
        <div className='app-body'>
          {children}
        </div>
        <AlertList
          position='top-right'
          alerts={notifications}
          timeout={NOTIFICATION_TIMEOUT}
          onDismiss={popNotification}
        />
        <Modal />
      </div>
    )
  }
}

App.childContextTypes = {
  language: PropTypes.string
}

App.propTypes = {
  loaders: PropTypes.object,
  history: PropTypes.object,
  location: PropTypes.object,
  children: PropTypes.element,
  language: PropTypes.string,
  notifications: PropTypes.array,
  popNotification: PropTypes.func,
  popAllNotifications: PropTypes.func,
  updateViewPort: PropTypes.func,
  isMobile: PropTypes.bool
}

const mapStateToProps = state => ({
  loaders: state.setup.loaders,
  language: state.setup.language,
  notifications: state.setup.notifications,
  isMobile: state.setup.isMobile
})

export default withRouter(connect(mapStateToProps, {
  popNotification,
  updateViewPort,
  popAllNotifications
})(App))
