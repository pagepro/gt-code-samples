import * as actions from 'platform/setup/actionTypes'
import { COMPLETE } from 'platform/setup/boardingStatuses'

export const defaultState = {
  isBoarded: false,
  step: null,
  isLoggedIn: false,
  connect: {
    linkedIn: false,
    xing: false
  },
  newsletter: false,
  checkedAuthorization: false,
  progress: {}
}

export default (state = defaultState, action) => {
  const { type, payload } = action
  switch (type) {
    case actions.LOGIN_USER:
      return {
        ...state,
        isLoggedIn: payload,
        checkedAuthorization: true
      }
    case actions.BOARD_USER:
      return {
        ...state,
        isBoarded: payload.onBoardingStatus === COMPLETE,
        step: payload.onBoardingStatus
      }
    case actions.CONNECT_LINKEDIN:
      return {
        ...state,
        connect: {
          ...state.connect,
          linkedIn: payload,
          xing: false
        }
      }
    case actions.CONNECT_XING:
      return {
        ...state,
        connect: {
          ...state.connect,
          xing: payload,
          linkedIn: false
        }
      }
    case actions.CONNECT_NEWSLETTER:
      return {
        ...state,
        newsletter: !state.newsletter
      }

    case actions.UPDATE_BOARDING_PROGRESS:
      return {
        ...state,
        progress: payload
      }

    case actions.SIGN_OUT:
      return defaultState

    default:
      return state
  }
}

export const getLoggedInInfo = state => state.boarding.isLoggedIn
