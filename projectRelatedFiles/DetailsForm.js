import React from 'react'
import PropTypes from 'prop-types'
import { Field, reduxForm } from 'redux-form'
import FormsButtons from 'platform/candidate/shared/FormsButtons'
import TextField from 'platform/forms/TextField'
import CandidateTypeSearch from 'platform/candidate/employment/CandidateTypeSearch'
import { CANDIDATE_EDIT_DETAILS_FORM } from 'platform/setup/formTypes'
import withLanguage from 'common/withLanguage'
import { getFocusField } from 'platform/candidate/progressBar/utils'
import {
  LOCATION
} from 'platform/candidate/progressBar/focusFields'

class EditDetailsForm extends React.Component {
  constructor () {
    super()

    this.changeHiddenFields = this.changeHiddenFields.bind(this)
  }

  get shouldFocusName () {
    return !this.shouldFocusLocation
  }

  get shouldFocusLocation () {
    return !this.dropdownInitialValue || getFocusField() === LOCATION
  }

  get dropdownInitialValue () {
    const {
      initialValues: {
        homeCityCountry,
        homeCityName,
        homeCityCountryRegion
      }
    } = this.props

    return homeCityCountry
      ? `${homeCityName}, ${homeCityCountryRegion}, ${homeCityCountry}`
      : ''
  }

  changeHiddenFields ({ item: location = {} } = {}) {
    const {
      change
    } = this.props

    change('homeCityCountry', location.country || '')
    change('homeCityId', location.id || '')
    change('homeCityName', location.name || '')
  }

  render () {
    const {
      handleSubmit,
      searchLocations,
      typeSearchId,
      formValues,
      getMessages
    } = this.props
    const {
      firstName,
      lastName,
      location,
      locationTypeCityTownHere
    } = getMessages()

    const dropdownInitialValue = this.dropdownInitialValue

    return (
      <form
        className='f-form'
        onSubmit={handleSubmit}
      >
        <fieldset className='f-form__fieldset'>
          <div className='f-form__body'>
            <div className='f-form__row'>
              <Field
                autoFocus={this.shouldFocusName}
                name='firstName'
                id='firstName'
                className='f-field__control'
                label={firstName}
                component={TextField}
                required
                floated
                />
            </div>
            <div className='f-form__row'>
              <Field
                name='lastName'
                id='lastName'
                className='f-field__control'
                label={lastName}
                component={TextField}
                required
                floated
              />
            </div>
            <div className='f-form__row'>
              <CandidateTypeSearch
                autoFocus={this.shouldFocusLocation}
                id={typeSearchId}
                label={formValues.homeCityId ? location : locationTypeCityTownHere}
                fetchOptionsMethod={searchLocations}
                initialSuggestion={dropdownInitialValue ? {
                  name: dropdownInitialValue
                } : null}
                onChange={this.changeHiddenFields}
                clearInputOnSelect
                isSingleSelect
              />
              <Field
                hidden
                name='homeCityCountry'
                id='homeCityCountry'
                component={TextField}
              />
              <Field
                hidden
                name='homeCityId'
                id='homeCityId'
                component={TextField}
              />
              <Field
                hidden
                name='homeCityName'
                id='homeCityName'
                component={TextField}
              />
            </div>
          </div>
          <FormsButtons
            isFinish
          />
        </fieldset>
      </form>
    )
  }
}

EditDetailsForm.propTypes = {
  handleSubmit: PropTypes.func,
  searchLocations: PropTypes.func,
  change: PropTypes.func,
  typeSearchId: PropTypes.string,
  initialValues: PropTypes.object,
  formValues: PropTypes.object,
  getMessages: PropTypes.func
}

export default reduxForm({
  form: CANDIDATE_EDIT_DETAILS_FORM,
  enableReinitialize: true
})(withLanguage(EditDetailsForm))
