import {
  STARTED,
  IMPORTING_LINKEDIN,
  IMPORTING_DOCUMENT,
  IMPORTING_XING,
  PRE_IMPORTING_LINKEDIN,
  NOT_STARTED,
  SKILLS,
  boardingRoutes,
  SOURCE_XING,
  SOURCE_LINKEDIN,
  SOURCE_CV,
  COMPLETE
} from 'platform/setup/boardingStatuses'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import {
  Route,
  Switch,
  Redirect
} from 'react-router-dom'
import { withRouter } from 'react-router'
import CandidateHeader from 'platform/candidate/layout/Header'
import ConnectForm from 'platform/boarding/ConnectForm'
import ConnectSkillsForm from 'platform/boarding/ConnectSkillsForm'
import ConnectDocument from 'platform/boarding/ConnectDocument'
import PreLinkedIn from 'platform/boarding/PreLinkedIn'
import { logOut } from 'platform/signIn/SignInActions'
import Xing from 'platform/boarding/Xing'
import ConnectExternalSource from 'platform/boarding/ConnectExternalSource'
import NotStarted from 'platform/boarding/NotStarted'
import Logo from 'platform/icons/ColorLogo'
import config from 'platform/setup/config'
import withLanguage from 'common/withLanguage'
const { routes } = config

class ConnectPage extends Component {
  componentDidMount () {
    const {
      boardingStep,
      history
    } = this.props

    if (boardingStep === COMPLETE) {
      history.push(config.routes.candidate)
    }
  }

  get intro () {
    const startedWithLinkedIn = false
    const {
      location,
      boardingStep,
      boardingSource,
      getMessages
    } = this.props

    const {
      createYourProfileWith,
      completeMoreOfYourProfileFromYourLinkedInAccount,
      letsGetStartedWithSomeQuestions,
      importingYourXingProfile,
      importingYourLinkedinProfile,
      pleaseTellUsBitMoreAboutYourself
    } = getMessages()

    const processingIntros = {
      [SOURCE_XING]: importingYourXingProfile,
      [SOURCE_LINKEDIN]: importingYourLinkedinProfile,
      [SOURCE_CV]: String.empty
    }

    let backupIntro = letsGetStartedWithSomeQuestions

    const intros = {
      [boardingRoutes[STARTED]]: letsGetStartedWithSomeQuestions,
      [boardingRoutes[SKILLS]]: pleaseTellUsBitMoreAboutYourself,
      [boardingRoutes[IMPORTING_XING]]: importingYourXingProfile,
      [boardingRoutes[IMPORTING_LINKEDIN]]: importingYourLinkedinProfile,
      [boardingRoutes[PRE_IMPORTING_LINKEDIN]]: importingYourLinkedinProfile,
      [boardingRoutes[IMPORTING_DOCUMENT]]: String.empty,
      [boardingRoutes[NOT_STARTED]]: startedWithLinkedIn
        ? completeMoreOfYourProfileFromYourLinkedInAccount
        : createYourProfileWith,
      [routes.connectProgress]: processingIntros[boardingSource]
    }

    if (boardingStep === IMPORTING_LINKEDIN) {
      backupIntro = importingYourLinkedinProfile
    }
    if (boardingStep === IMPORTING_XING) {
      backupIntro = importingYourXingProfile
    }

    return typeof intros[location.pathname] !== 'undefined' ? intros[location.pathname] : backupIntro
  }

  render () {
    const {
      logOut,
      history,
      location,
      getMessages
    } = this.props
    const {
      buildYourProfile
    } = getMessages()

    const withHeader = (
      location.pathname === boardingRoutes[STARTED] ||
      location.pathname === boardingRoutes[SKILLS]
    )

    return (
      <div className='l-bg l-bg--white l-bg-onboarding'>
        <CandidateHeader
          logOut={logOut}
          history={history}
        />
        <div className='c-logo c-logo--tablet'>
          <Logo />
        </div>
        <div className='l-centered-content'>
          <div className='l-inner'>
            {this.intro && (
              <div className='c-hero'>
                <div className='c-hero__info'>
                  {withHeader
                    ? (
                      <p className='t-heading-2-new t-upper t-weight-light'>
                        {buildYourProfile}
                      </p>
                    )
                    : null
                  }
                  <p className='t-heading-5 t-weight-bold'>
                    {this.intro}
                  </p>
                </div>
              </div>
              )
            }
            <Switch>
              <Route
                path={routes.preConnectLinkedIn}
                component={PreLinkedIn}
              />
              <Route
                path={routes.connectForm}
                component={ConnectForm}
              />
              <Route
                path={routes.connectDocument}
                component={ConnectDocument}
              />
              <Route
                path={routes.connectSkillsForm}
                component={ConnectSkillsForm}
              />
              <Route
                path={routes.connectXing}
                component={Xing}
              />
              <Route
                path={routes.connectProgress}
                component={ConnectExternalSource}
              />
              <Route
                path={routes.connectLinkedIn}
                component={ConnectExternalSource}
              />
              <Route
                path={routes.connectNotStarted}
                component={NotStarted}
              />
              <Redirect from={routes.connect} to={routes.connectNotStarted} />
            </Switch>
          </div>
        </div>
      </div>
    )
  }
}

ConnectPage.propTypes = {
  history: PropTypes.object,
  logOut: PropTypes.func,
  isLoggedIn: PropTypes.bool,
  isBoarded: PropTypes.bool,
  boardingStep: PropTypes.string,
  boardingSource: PropTypes.string,
  location: PropTypes.object,
  getMessages: PropTypes.func
}

const mapStateToProps = state => ({
  isLoggedIn: state.boarding.isLoggedIn,
  isBoarded: state.boarding.isBoarded,
  boardingStep: state.boarding.step,
  boardingSource: state.boarding.progress.source
})

export default withRouter(connect(mapStateToProps, {
  logOut
})(withLanguage(ConnectPage)))
