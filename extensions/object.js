// eslint-disable
Object.tryGetProperty = (
  Object.tryGetProperty || ((obj = {}, propertyAccessor = Function.empty, defaultValue) => {
    try {
      return propertyAccessor(obj)
    } catch (e) {
      return defaultValue
    }
  })
)

Object.empty = Object.freeze({})

Object.isEmpty = (
  Object.isEmpty || ((obj = {}) => {
    return JSON.stringify(obj) === '{}'
  })
)
