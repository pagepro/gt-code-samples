/* eslint-disable */
String.prototype.format = String.prototype.format || (function () {
  var args = arguments
  return this.replace(/{(\d+)}/g, (match, number) => {
    return typeof args[number] !== 'undefined'
      ? args[number]
      : match
  })
})

String.prototype.replaceAll = String.prototype.replaceAll || (function (search, replacement, caseInsensitive = false) {
  return this.replace(new RegExp(search, `g${caseInsensitive ? 'i' : ''}`), replacement)
})

String.prototype.splice = String.prototype.splice || (function (start, delCount, newSubStr) {
  return this.slice(0, start) + newSubStr + this.slice(start + Math.abs(delCount))
})

String.prototype.insert = String.prototype.insert || (function (index = 0, content) {
  return this.splice(index, 0, content)
})

String.prototype.firstCharToUpperCase = String.prototype.firstCharToUpperCase || (function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
})

String.prototype.firstCharToLowerCase = String.prototype.firstCharToLowerCase || (function() {
  return this.charAt(0).toLowerCase() + this.slice(1);
})

String.prototype.allWordsToUpperCase = String.prototype.allWordsToUpperCase || (function () {
  return this.split(' ').map(word => word.firstCharToUpperCase()).join(' ')
})

Object.defineProperty(String, 'empty', {
  value: ''
})

Object.defineProperty(String, 'newLine', {
  value: '\n'
})
