/* eslint-disable */
Array.prototype.remove = Array.prototype.remove || (function (callback) {
  const itemToRemoveIndex = this.findIndex(callback)
  const itemToRemove = this[itemToRemoveIndex]
  if (~itemToRemoveIndex) this.splice(itemToRemoveIndex, 1)
  return itemToRemove
})

Array.prototype.removeAll = Array.prototype.removeAll || (function (callback) {
  const removedItems = []
  let indexToRemove = this.findIndex(callback)
  while (~indexToRemove) {
    removedItems.push(this[indexToRemove])
    this.splice(indexToRemove, 1)
    indexToRemove = this.findIndex(callback)
  }

  return removedItems
})

Array.prototype.insert = Array.prototype.insert || (function (value, index) {
  this.splice(index, 0, value)
  return value
})

Array.prototype.without = Array.prototype.without || (function (
  itemsToRemove,
  callback = (item1, item2) => item1 === item2
) {
  return this.filter(item => {
    const shallowCopy = [ ...itemsToRemove ]
    const index = shallowCopy.findIndex(innerItem => callback(item, innerItem))
    if (~index) {
      shallowCopy.slice(index, 1)
    }
    return !~index
  })
})
