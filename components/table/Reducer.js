import * as actions from 'admin/setup/actionTypes'

const sortData = sortKey => {
  const hasMinusOnFront = sortKey.startsWith('-')
  const direction = sortKey.startsWith('-') ? 'desc' : 'asc'
  const key = hasMinusOnFront ? sortKey.slice(1) : sortKey
  return (a, b) => {
    if (direction === 'asc') {
      return b[key] > a[key]
    } else {
      return a[key] > b[key]
    }
  }
}

const updateOnlyWithId = (newItem, id) => item => {
  return id === item.id ? newItem : item
}

const updateOnlyOneFieldWithId = (id, field, value) => item => {
  return id === item.id
    ? {
      ...item,
      [field]: value
    }
    : item
}

export const defaultState = {}

export default (state = defaultState, action) => {
  const { type, payload } = action

  switch (type) {
    case actions.TOGGLE_TABLE_LOADER:
      return {
        ...state,
        [payload.key]: {
          ...state[payload.key],
          isLoading: payload.value
        }
      }
    case actions.INIT_TABLE:
      return {
        ...state,
        [payload.key]: {
          data: [ ...payload.data ].sort(sortData(payload.sortBy || payload.columns[0].name)),
          columns: payload.columns,
          selected: {},
          sortBy: payload.sortBy || payload.columns[0].name,
          isLoading: false,
          editItem: null
        }
      }
    case actions.CLEAR_TABLE:
      return {
        ...state,
        [payload]: {
          ...state[payload],
          data: [],
          selected: {},
          editItem: null
        }
      }
    case actions.DESTROY_TABLE:
      const {
        [payload]: currentTable,
        ...previousTables
      } = state
      return previousTables
    case actions.UPDATE_TABLE_DATA:
      return {
        ...state,
        [payload.key]: {
          ...state[payload.key],
          data: payload.data.sort(sortData(state[payload.key].sortBy)),
          isLoading: false
        }
      }
    case actions.UPDATE_TABLE_CONFIG:
      return {
        ...state,
        [payload.key]: {
          ...state[payload.key],
          ...payload
        }
      }
    case actions.SORT_TABLE_DATA:
      return {
        ...state,
        [payload.key]: {
          ...state[payload.key],
          data: [ ...state[payload.key].data ].sort(sortData(payload.sortKey)),
          sortBy: payload.sortKey,
          isLoading: false
        }
      }
    case actions.OPEN_EDIT_MODE:
      return {
        ...state,
        [payload.key]: {
          ...state[payload.key],
          editItem: payload.item
        }
      }
    case actions.SAVE_EDITTED_DATA:
      return {
        ...state,
        [payload.key]: {
          ...state[payload.key],
          data: [ ...state[payload.key].data ]
            .map(updateOnlyWithId(payload.item, payload.id || payload.item.id)),
          editItem: null
        }
      }
    case actions.UPDATE_TABLE_ITEM:
      return {
        ...state,
        [payload.key]: {
          ...state[payload.key],
          data: [ ...state[payload.key].data ]
            .map(updateOnlyOneFieldWithId(payload.id, payload.field, payload.value))
        }
      }
    case actions.EDIT_TABLE_DATA:
      return {
        ...state,
        [payload.key]: {
          ...state[payload.key],
          editItem: {
            ...state[payload.key].editItem || {},
            [payload.item]: payload.value
          }
        }
      }
    case actions.ADD_SELECTED_TABLE_DATA:
      return {
        ...state,
        [payload.key]: {
          ...state[payload.key],
          selected: {
            ...state[payload.key].selected,
            [payload.data]: true
          }
        }
      }
    case actions.REMOVE_SELECTED_TABLE_DATA:
      return {
        ...state,
        [payload.key]: {
          ...state[payload.key],
          selected: {
            ...state[payload.key].selected,
            [payload.data]: false
          }
        }
      }
    case actions.ADD_TABLE_DATA:
      return {
        ...state,
        [payload.key]: {
          ...state[payload.key],
          data: [ ...state[payload.key].data, payload.data ],
          editItem: payload.data
        }
      }
    default:
      return state
  }
}
