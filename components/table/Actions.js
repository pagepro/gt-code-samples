import * as actions from 'admin/setup/actionTypes'

const initTableAction = tableConfig => ({
  type: actions.INIT_TABLE,
  payload: tableConfig
})

export const initTable = tableConfig => dispatch => {
  return tableConfig.method()
    .then(data => {
      dispatch(initTableAction({
        data,
        key: tableConfig.key,
        columns: tableConfig.columns,
        sortBy: tableConfig.sortBy
      }))
    })
}

const resetTableAction = key => ({
  type: actions.CLEAR_TABLE,
  payload: key
})

export const resetTable = key => dispatch => {
  dispatch(resetTableAction(key))
}

const destroyTableAction = key => ({
  type: actions.DESTROY_TABLE,
  payload: key
})

export const destroyTable = key => dispatch => {
  dispatch(destroyTableAction(key))
}

const updateTableItemAction = editItem => ({
  type: actions.UPDATE_TABLE_ITEM,
  payload: editItem
})

export const updateTableItem = editItem => dispatch => {
  dispatch(updateTableItemAction(editItem))
}

const updateTableDataAction = (key, data) => ({
  type: actions.UPDATE_TABLE_DATA,
  payload: { key, data }
})

export const updateTableData = ({ params, method, key }) => dispatch => {
  return method(params)
    .then(data => {
      dispatch(updateTableDataAction(key, data))
    })
}

const sortTableDataAction = data => ({
  type: actions.SORT_TABLE_DATA,
  payload: data
})

export const sortTableData = sortOptions => dispatch => {
  dispatch(sortTableDataAction(sortOptions))
}

const toggleTableLoaderAction = (key, value) => ({
  type: actions.TOGGLE_TABLE_LOADER,
  payload: { key, value }
})

export const toggleTableLoader = (key, value = true) => dispatch => {
  dispatch(toggleTableLoaderAction(key, value))
}

const openEditModeAction = config => ({
  type: actions.OPEN_EDIT_MODE,
  payload: config
})

const saveItemAction = (tableId, data, id) => ({
  type: actions.SAVE_EDITTED_DATA,
  payload: {
    key: tableId,
    item: data,
    id: id || data.id
  }
})

export const saveEdit = (tableId, data, id) => dispatch => {
  dispatch(saveItemAction(tableId, data, id))
}
export const openEdit = editConfig => dispatch => {
  dispatch(openEditModeAction(editConfig))
}
export const toggleUserActive = () => dispatch => {
  console.log('toggle active')
}

const editTableDataAction = data => ({
  type: actions.EDIT_TABLE_DATA,
  payload: data
})

export const editTableData = editedData => dispatch => {
  dispatch(editTableDataAction(editedData))
}

const addSelectedTableDataAction = data => ({
  type: actions.ADD_SELECTED_TABLE_DATA,
  payload: data
})

const removeSelectedTableDataAction = data => ({
  type: actions.REMOVE_SELECTED_TABLE_DATA,
  payload: data
})

export const addSelectedTableData = (id, data) => dispatch => {
  dispatch(addSelectedTableDataAction({key: id, data}))
}

export const removeSelectedTableData = (id, data) => dispatch => {
  dispatch(removeSelectedTableDataAction({key: id, data}))
}

const addRowAction = (key, data) => {
  return {
    type: actions.ADD_TABLE_DATA,
    payload: {
      key,
      data
    }
  }
}

export const addRow = (tableKey, data) => dispatch => {
  dispatch(addRowAction(tableKey, data))
}
