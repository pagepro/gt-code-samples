import React from 'react'
import PropTypes from 'prop-types'
import Checkbox from '../search/fields/Checkbox'
import classnames from 'classnames'

const TableColumn = props => {
  const {
    column,
    itemKey,
    editItem,
    isInEditMode,
    data,
    selected,
    toggleSelection
  } = props

  const cellClass = column.className ? ` ${column.className}` : ''
  return (
    <div
      className={classnames(
        'c-table__cell t-text-6 t-weight-regular', {
          [`${cellClass}`]: !!column.className
        }
      )}
      style={{ width: column.width }}
    >
      {isInEditMode && column.renderEditMethod
        ? column.renderEditMethod(editItem[column.name], editItem)
        : column.renderMethod
          ? column.renderMethod(data[column.name], data, isInEditMode)
          : column.name === 'checkbox'
            ? (
              <Checkbox
                input={{
                  onChange: toggleSelection(itemKey),
                  name: `toggle-${itemKey}`,
                  value: selected[itemKey] || false
                }}
                id={`toggle-${itemKey}`}
              />
            )
            : data[column.name]
      }
    </div>
  )
}

TableColumn.propTypes = {
  column: PropTypes.object,
  itemKey: PropTypes.string,
  editItem: PropTypes.object,
  isInEditMode: PropTypes.bool,
  data: PropTypes.object,
  selected: PropTypes.object,
  toggleSelection: PropTypes.func
}

export default TableColumn
