import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import ArrowDown from 'admin/icons/ArrowDown'
import classnames from 'classnames'
import {
  initTable,
  updateTableData,
  destroyTable,
  sortTableData,
  toggleTableLoader,
  addSelectedTableData,
  removeSelectedTableData
} from 'admin/tables/TablesActions'
import TableColumn from './TableColumn'
import SmallLoader from 'common/SmallLoader'
import {
  sortDesc,
  sortAsc
} from 'utils/helpers'

class Table extends React.Component {
  constructor () {
    super()

    this.renderCell = this.renderCell.bind(this)
    this.renderHeadCell = this.renderHeadCell.bind(this)
    this.renderRow = this.renderRow.bind(this)
    this.sortBy = this.sortBy.bind(this)
    this.toggleSelection = this.toggleSelection.bind(this)
  }

  componentDidMount () {
    const {
      id,
      columns,
      isSelectable,
      initTable,
      updateTableData,
      table,
      getMethod,
      sortByKey,
      toggleTableLoader
    } = this.props
    toggleTableLoader(this.props.id, true)
    if (table) {
      updateTableData({ method: getMethod, key: id })
    } else {
      const tableColumns = isSelectable
        ? [
          {
            label: '',
            name: 'checkbox',
            width: '50px'
          },
          ...columns
        ]
        : columns
      initTable({
        key: id,
        columns: tableColumns,
        method: getMethod,
        sortBy: sortByKey
      })
    }
  }

  componentWillUnmount () {
    this.props.destroyTable(this.props.id)
  }

  sortBy (sortKey) {
    const {
      id,
      table: {
        sortBy: currentSortName
      },
      sortTableData
    } = this.props

    return () => {
      sortTableData({
        key: id,
        sortKey: currentSortName === sortKey
        ? `-${sortKey}`
        : sortKey
      })
    }
  }

  getDirectionClass (item) {
    const { sortBy = '' } = this.props.table || {}
    const isDesc = sortBy.startsWith('-')

    if (sortBy.includes(item)) {
      return isDesc ? ' is-desc' : ' is-asc'
    }

    return ''
  }

  renderHeadCell (item) {
    const directionClass = this.getDirectionClass(item.name) || ''

    return (
      <div
        key={`t-head-${this.props.id}-${item.label}`}
        className={classnames(
          'c-table__heading',
          {
            'c-table__heading-content--filter': item.isSortable,
            [`${item.headerClassName}`]: !!item.headerClassName
          }
        )}
        style={{ width: item.width }}
      >
        <div className='c-table__action'>
          <a
            className={classnames(
              'c-cta t-heading-6 t-weight-regular u-paint-4 t-upper',
              `${directionClass}`,
              {
                'c-cta--table-filter': item.isSortable
              }
            )}
            onClick={item.isSortable ? this.sortBy(item.name) : null}
          >
            {item.label}
            {directionClass && <ArrowDown />}
          </a>
        </div>
      </div>
    )
  }

  toggleSelection (itemId) {
    const {
      id: tableId,
      table: {
        selected
      } = {},
      addSelectedTableData,
      removeSelectedTableData
    } = this.props

    return () => {
      if (selected[itemId]) {
        removeSelectedTableData(tableId, itemId)
      } else {
        addSelectedTableData(tableId, itemId)
      }
    }
  }

  renderCell (data, isInEditMode) {
    const {
      table: {
        selected,
        editItem
      } = {},
      getKeyRowMethod
    } = this.props

    const itemKey = getKeyRowMethod(data)

    return column => (
      <TableColumn
        itemKey={itemKey}
        selected={selected}
        editItem={editItem}
        data={data}
        column={column}
        isInEditMode={isInEditMode}
        toggleSelection={this.toggleSelection}
        key={`t-cell-${this.props.id}-${column.name}-${itemKey}`}
      />
      )
  }

  renderRow (item) {
    const {
      table: {
        columns,
        editItem
      },
      id,
      getKeyRowMethod
    } = this.props

    const isCurrentEditing = editItem && editItem.id === item.id
    const body = (
      <div className='c-table__row u-bg-2 u-bg-10-on-hover'>
        {columns.map(this.renderCell(item, isCurrentEditing))}
      </div>
    )
    return (
      <li
        className={classnames(
          'c-table__body-item',
          {
            'is-edited': isCurrentEditing
          }
        )}
        key={`t-row-${id}-${getKeyRowMethod(item)}`}
      >
        {isCurrentEditing
          ? (
            <form className='f-form f-form--table'>
              <fieldset className='f-form__fieldset'>
                <div className='f-form__body'>
                  {body}
                </div>
              </fieldset>
            </form>
          )
          : body
        }
      </li>
    )
  }

  get sortKey () {
    const {
      table: {
        sortBy = ''
      } = {}
    } = this.props

    if (sortBy.startsWith('-')) {
      return sortBy.slice(1)
    }
    return sortBy
  }

  get tableData () {
    const {
      table: {
        data = [],
        sortBy = ''
      } = {}
    } = this.props

    const keyExtractor = item => item[this.sortKey]

    return sortBy
      ? data.sort(sortBy.startsWith('-')
        ? sortAsc(keyExtractor)
        : sortDesc(keyExtractor)
      )
      : data
  }

  render () {
    const {
      withoutHeader,
      table: {
        data = [],
        isLoading,
        columns: tableColumns = []
      } = {}
    } = this.props

    return (
      <div className='c-table-container'>
        <div className='c-table'>
          {isLoading && (
            <div className='c-table-loader'>
              <SmallLoader />
            </div>
          )}
          {!withoutHeader && (
            <div className='c-table__head'>
              <div className='c-table__row u-flex-space-between--top'>
                {tableColumns.map(this.renderHeadCell)}
              </div>
            </div>
          )}
          <ul className='c-table__body'>
            {data.map(this.renderRow)}
          </ul>
        </div>
      </div>
    )
  }
}

Table.propTypes = {
  getKeyRowMethod: PropTypes.func,
  getMethod: PropTypes.func.isRequired,
  columns: PropTypes.array.isRequired,
  id: PropTypes.string.isRequired,
  withoutHeader: PropTypes.bool,
  sortByKey: PropTypes.string,
  isSelectable: PropTypes.bool,
  table: PropTypes.object,
  initTable: PropTypes.func,
  destroyTable: PropTypes.func,
  updateTableData: PropTypes.func,
  sortTableData: PropTypes.func,
  toggleTableLoader: PropTypes.func,
  addSelectedTableData: PropTypes.func,
  removeSelectedTableData: PropTypes.func
}

Table.defaultProps = {
  getKeyRowMethod: item => item.id
}

const mapStateToProps = (state, props) => ({
  table: state.tables[props.id]
})

export default connect(mapStateToProps, {
  initTable,
  destroyTable,
  updateTableData,
  sortTableData,
  toggleTableLoader,
  addSelectedTableData,
  removeSelectedTableData
})(Table)
