import React from 'react'
import PropTypes from 'prop-types'
import {
  tryToGetImage
} from 'utils/images'

const defaultErrorTemplate = (
  <span className='c-icon-wrapper'>
    <svg className='o-icon o-icon--image' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlnsXlink='http://www.w3.org/1999/xlink' x='0px' y='0px'
      viewBox='0 0 512 512' enableBackground='new 0 0 512 512' xmlSpace='preserve'>
      <g>
        <g>
          <path d={`M439.026,0H72.975C32.737,0,0,32.737,0,72.975v366.05C0,479.264,32.737,512,72.975,512h366.05
            C479.264,512,512,479.264,512,439.026V72.975C512,32.737,479.264,0,439.026,0z M470.636,439.025
            c0,17.43-14.181,31.611-31.61,31.611H72.975c-17.43,0-31.611-14.181-31.611-31.61V72.975c0-17.43,14.181-31.611,31.611-31.611
            h366.05c17.43,0,31.611,14.181,31.611,31.611V439.025z`} />
        </g>
      </g>
      <g>
        <g>
          <path d={`M164.487,104.585c-33.031,0-59.902,26.872-59.902,59.902s26.872,59.902,59.902,59.902s59.902-26.871,59.902-59.902
            S197.518,104.585,164.487,104.585z M164.487,183.026c-10.222,0-18.538-8.316-18.538-18.538s8.316-18.538,18.538-18.538
            s18.538,8.316,18.538,18.538S174.709,183.026,164.487,183.026z`} />
        </g>
      </g>
      <g>
        <g>
          <path d={`M505.942,319.814L375.21,189.083c-8.077-8.077-21.172-8.077-29.249,0l-287.61,287.61c-8.077,8.077-8.077,21.172,0,29.249
            c4.038,4.039,9.332,6.058,14.625,6.058c5.293,0,10.586-2.019,14.624-6.058l272.986-272.986l116.108,116.107
            c8.077,8.077,21.172,8.077,29.249,0C514.019,340.987,514.019,327.891,505.942,319.814z`} />
        </g>
      </g>
    </svg>
  </span>
)

class ImageLoader extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      imageLoadFailed: false,
      isLoading: true
    }
    this.hasUnmounted = false

    this.onLoadFailed = this.onLoadFailed.bind(this)
    this.onLoadSuccess = this.onLoadSuccess.bind(this)
    this.checkImage = this.checkImage.bind(this)
  }

  onLoadFailed () {
    if (this.hasUnmounted) {
      return
    }
    this.setState({
      imageLoadFailed: true,
      isLoading: false
    })
  }

  onLoadSuccess () {
    if (this.hasUnmounted) {
      return
    }
    this.setState({
      imageLoadFailed: false,
      isLoading: false
    })
  }

  checkImage (src) {
    tryToGetImage(src)
      .then(this.onLoadSuccess, this.onLoadFailed)
  }

  componentDidMount () {
    this.hasUnmounted = false
    const {
      src
    } = this.props
    if (!src) {
      this.setState({ isLoading: false })
    } else {
      this.checkImage(src)
    }
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.src !== nextProps.src) {
      this.checkImage(nextProps.src)
    }
  }

  componentWillUnmount () {
    this.hasUnmounted = true
  }

  render () {
    const {
      children = defaultErrorTemplate,
      ...props
    } = this.props

    const {
      imageLoadFailed
    } = this.state
    // TODO: we can add loader for images
    return this.state.isLoading || imageLoadFailed || !this.props.src
        ? children
        : (
          <img
            ref={ref => this.imgRef}
            {...props}
          />
        )
  }
}

ImageLoader.propTypes = {
  children: PropTypes.node,
  src: PropTypes.string
}

export default ImageLoader
