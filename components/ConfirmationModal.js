import React from 'react'
import PropTypes from 'prop-types'
import BasicModalBase from 'platform/modals/bases/BasicModalBase'
import { preventDefault } from 'utils/helpers'
import withLanguage from 'common/withLanguage'

class ConfirmationModal extends React.Component {
  constructor () {
    super()

    this.confirmButton = null
    this.onAbort = this.onAbort.bind(this)
    this.onAccept = this.onAccept.bind(this)
    this.onCancel = this.onCancel.bind(this)
  }

  componentDidMount () {
    this.confirmButton.focus()
  }

  onAbort () {
    const {
      closeModal,
      onCancel
    } = this.props
    closeModal()
    onCancel && onCancel()
  }

  onAccept () {
    const {
      closeModal,
      onSubmit
    } = this.props
    closeModal()
    onSubmit && onSubmit()
  }

  onCancel () {
    const { onClose } = this.props
    if (onClose) {
      onClose()
      return
    }
    this.onAbort()
  }

  render () {
    const {
      okLabel,
      cancelLabel,
      description,
      getMessages
    } = this.props
    const {
      ok: okLabelDefault,
      cancel: cancelLabelDefault
    } = getMessages()

    return (
      <BasicModalBase
        onCancel={this.onCancel}
      >
        <div className='c-modal__text'>
          <p className='t-heading-3 t-weight-medium u-paint-29'>
            {description}
          </p>
        </div>
        <div className='c-modal__action c-modal__action--two-in-row'>
          <a
            tabIndex='100'
            href='#'
            onClick={preventDefault(this.onAccept)}
            ref={ref => { this.confirmButton = ref }}
            className='c-btn c-btn--modal c-btn--confirm'
          >
            {okLabel || okLabelDefault}
          </a>
          <a
            href='#'
            tabIndex='101'
            onClick={preventDefault(this.onAbort)}
            className='c-btn c-btn--modal c-btn--cancel'
          >
            {cancelLabel || cancelLabelDefault}
          </a>
        </div>
      </BasicModalBase>
    )
  }
}

ConfirmationModal.propTypes = {
  okLabel: PropTypes.string,
  cancelLabel: PropTypes.string,
  onCancel: PropTypes.func,
  description: PropTypes.string,
  closeModal: PropTypes.func,
  onClose: PropTypes.func,
  onSubmit: PropTypes.func,
  getMessages: PropTypes.func
}

export default withLanguage(ConfirmationModal)
