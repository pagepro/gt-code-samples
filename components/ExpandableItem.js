import React from 'react'

const handleCallback = cb => () => {
  if (cb && typeof cb === 'function') {
    cb()
  }
}

class ExpandableItem extends React.Component {
  constructor () {
    super()

    this.state = {
      isOpened: false
    }

    this.parentRef = null

    this.show = this.show.bind(this)
    this.hide = this.hide.bind(this)
    this.toggle = this.toggle.bind(this)
    this.hideOnClickOutside = this.hideOnClickOutside.bind(this)
  }

  show (cb) {
    this.setState({
      isOpened: true
    }, handleCallback(cb))
  }

  hide (cb) {
    if (this.state.isOpened) {
      this.setState({
        isOpened: false
      }, handleCallback(cb))
    }
  }

  toggle (cb) {
    this.setState(previousState => ({
      isOpened: !previousState.isOpened
    }), handleCallback(cb))
  }

  hideOnClickOutside (event) {
    if (this.parentRef && !this.parentRef.contains(event.target)) {
      this.hide()
    }
  }

  componentDidMount () {
    if (this.parentRef) {
      window.addEventListener('click', this.hideOnClickOutside)
    }
  }

  componentWillUnmount () {
    if (this.parentRef) {
      window.removeEventListener('click', this.hideOnClickOutside)
    }
  }
}

export default ExpandableItem
