import React from 'react'
import PropTypes from 'prop-types'
import withLanguage from 'common/withLanguage'
import classnames from 'classnames'

const BasicModalBase = props => {
  const {
    className: classNameProp,
    bodyClassName,
    hideBackButton,
    getMessages,
    title,
    onCancel,
    children
  } = props
  const {
    back
  } = getMessages()

  const className = classNameProp ? ` ${classNameProp}` : ''
  return (
    <div className={`c-modal__wrapper u-bg-2 ui-bg-white${className}`}>
      <div className='c-modal__head'>
        {!!title && (
          <div className='c-modal__title'>
            <h2 className='t-heading-2-new t-weight-regular u-paint-20'>
              {title}
            </h2>
          </div>
        )}
        {!hideBackButton && (
          <div className='c-modal__close'>
            <a
              onClick={onCancel}
              className={classnames('c-link c-link--text', {
                'is-disabled': !onCancel
              })}
            >
              {back}
            </a>
          </div>
        )}
      </div>
      <div className={`c-modal__body ${bodyClassName || ''}`}>
        <div className='c-modal__content'>
          {children}
        </div>
      </div>
    </div>
  )
}

BasicModalBase.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string,
  className: PropTypes.string,
  bodyClassName: PropTypes.string,
  onCancel: PropTypes.func,
  hideBackButton: PropTypes.bool,
  getMessages: PropTypes.func
}

export default withLanguage(BasicModalBase)
