import React from 'react'
import PropTypes from 'prop-types'
import TabsNavigationItem from 'common/tabs/TabsNavigationItem'

class TabsNavigation extends React.Component {
  constructor () {
    super()

    this.getFilteredChildren = this.getFilteredChildren.bind(this)
    this.applyPropsToChild = this.applyPropsToChild.bind(this)
  }

  applyPropsToChild (child) {
    return React.cloneElement(
      child,
      {
        tabsGroupId: this.props.tabsGroupId
      }
    )
  }

  getFilteredChildren () {
    const childrenAsArray = React.Children.toArray(this.props.children)
    const onlyTabsNavigationChildren = childrenAsArray.reduce((result, child) => {
      if (child.type === TabsNavigationItem) {
        result.push(this.applyPropsToChild(child))
      }

      return result
    }, [])

    if (onlyTabsNavigationChildren.length !== childrenAsArray.length) {
      console.warn('TabsNavigation Component contains children with types other than TabsNavigationItem')
    }

    return onlyTabsNavigationChildren
  }

  render () {
    return this.getFilteredChildren()
  }
}

TabsNavigation.propTypes = {
  tabsGroupId: PropTypes.string,
  children: PropTypes.node
}

export default TabsNavigation
