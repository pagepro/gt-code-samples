import React from 'react'
import PropTypes from 'prop-types'
import {
  connect
} from 'react-redux'
import {
  isTabActive
} from 'common/tabs/Actions'

class Tab extends React.Component {
  render () {
    const {
      preventRerender,
      children,
      name,
      tabsGroupId,
      isTabActive,
      className
    } = this.props
    const isTabActiveValue = isTabActive(tabsGroupId, name)

    return (preventRerender || isTabActiveValue) ? (
      <div
        style={(preventRerender && !isTabActiveValue)
          ? {
            display: 'none'
          }
          : {}
        }
        className={className}
      >
        {preventRerender
          ? children
          : isTabActiveValue ? children : null
        }
      </div>
    ) : null
  }
}

Tab.propTypes = {
  preventRerender: PropTypes.bool,
  children: PropTypes.node,
  tabsGroupId: PropTypes.string,
  className: PropTypes.string,
  name: PropTypes.string.isRequired,
  tabsGroups: PropTypes.object,
  isTabActive: PropTypes.func
}

Tab.propTypes = {
  className: PropTypes.string
}

// Used by react-redux to know when this slice of state changes
// and when component should rerender
const mapStateToProps = state => ({
  tabsGroups: state.tabsGroups
})

export default connect(mapStateToProps, {
  isTabActive
})(Tab)
