import React from 'react'
import PropTypes from 'prop-types'
import {
  connect
} from 'react-redux'
import {
  initTabsGroup
} from 'common/tabs/Actions'
import Tab from 'common/tabs/Tab'

class Tabs extends React.Component {
  constructor () {
    super()

    this.getFilteredChildren = this.getFilteredChildren.bind(this)
    this.applyPropsToChild = this.applyPropsToChild.bind(this)
    this.getTabsNames = this.getTabsNames.bind(this)
  }

  componentWillMount () {
    const {
      id,
      initTabsGroup,
      shouldNotReinit,
      activeTab
    } = this.props

    const tabsNames = this.getTabsNames()
    const defaultActiveTab = shouldNotReinit ? activeTab : ''

    initTabsGroup({
      id,
      tabsNames,
      activeTab: defaultActiveTab || tabsNames[0] || ''
    })
  }

  getTabsNames () {
    return React.Children.map(this.props.children, ({ props: { name } }) => name)
  }

  applyPropsToChild (child) {
    return React.cloneElement(
      child,
      {
        tabsGroupId: this.props.id
      }
    )
  }

  getFilteredChildren () {
    const childrenAsArray = React.Children.toArray(this.props.children)
    const onlyTabChildren = childrenAsArray.reduce((result, child) => {
      if (child.type === Tab) {
        result.push(this.applyPropsToChild(child))
      }

      return result
    }, [])

    if (onlyTabChildren.length !== childrenAsArray.length) {
      console.warn('Tabs Component contains children with types other than Tab')
    }

    return onlyTabChildren
  }

  render () {
    return this.getFilteredChildren()
  }
}

Tabs.propTypes = {
  id: PropTypes.string.isRequired,
  children: PropTypes.node,
  initTabsGroup: PropTypes.func,
  shouldNotReinit: PropTypes.bool,
  activeTab: PropTypes.string
}

const mapStateToProps = (state, props) => ({
  activeTab: (state.tabsGroups[props.id] || {}).activeTab
})

export default connect(mapStateToProps, {
  initTabsGroup
})(Tabs)
