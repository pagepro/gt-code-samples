import {
  SET_ACTIVE_TAB,
  INIT_TABS_GROUP
} from 'common/tabs/Actions'

const defaultState = {}

export default (state = defaultState, action) => {
  const { type, payload } = action
  switch (type) {
    case SET_ACTIVE_TAB:
      return {
        ...state,
        [payload.id]: {
          ...state[payload.id],
          activeTab: payload.tabName
        }
      }
    case INIT_TABS_GROUP:
      const {
        id,
        ...tabsGroupData
      } = payload
      return {
        ...state,
        [id]: {
          ...tabsGroupData
        }
      }
    default:
      return state
  }
}
