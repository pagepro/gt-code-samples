import React from 'react'
import PropTypes from 'prop-types'
import {
  connect
} from 'react-redux'
import {
  setActiveTab,
  isTabActive
} from 'common/tabs/Actions'

class TabsNavigationItem extends React.Component {
  constructor () {
    super()

    this.setAsActive = this.setAsActive.bind(this)
  }

  setAsActive () {
    const {
      tabName,
      tabsGroupId,
      setActiveTab,
      isTabActive,
      allowToggle
    } = this.props
    if (isTabActive(tabsGroupId, tabName)) {
      if (!allowToggle) return false

      setActiveTab(tabsGroupId, String.empty)
      return true
    }
    setActiveTab(tabsGroupId, tabName)
    return true
  }

  render () {
    const {
      children,
      isTabActive,
      tabName,
      tabsGroupId
    } = this.props
    const isFunc = typeof children === 'function'
    return isFunc ? children(
      this.setAsActive,
      isTabActive(tabsGroupId, tabName)
    ) : children
  }
}

TabsNavigationItem.propTypes = {
  setActiveTab: PropTypes.func,
  isTabActive: PropTypes.func,
  tabsGroups: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.func
  ]),
  tabName: PropTypes.string.isRequired,
  tabsGroupId: PropTypes.string,
  allowToggle: PropTypes.bool
}

const mapStateToProps = state => ({
  tabsGroups: state.tabsGroups
})

export default connect(mapStateToProps, {
  setActiveTab,
  isTabActive
})(TabsNavigationItem)
