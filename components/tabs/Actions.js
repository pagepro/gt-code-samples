export const SET_ACTIVE_TAB = 'SET_ACTIVE_TAB'
export const INIT_TABS_GROUP = 'INIT_TABS_GROUP'

const setActiveTabAction = (tabsGroupId, tabName) => ({
  type: SET_ACTIVE_TAB,
  payload: {
    id: tabsGroupId,
    tabName
  }
})

export const setActiveTab = (tabsGroupId, tabName) => dispatch => {
  dispatch(setActiveTabAction(tabsGroupId, tabName))
}

const initTabsGroupAction = tabsGroup => ({
  type: INIT_TABS_GROUP,
  payload: tabsGroup
})

export const initTabsGroup = tabsGroup => dispatch => {
  dispatch(initTabsGroupAction(tabsGroup))
}

export const isTabActive = (tabsGroupId, tabName) => (_, getState) => {
  const {
    [tabsGroupId]: {
      activeTab = ''
    } = {}
  } = (getState().tabsGroups || {})
  return activeTab === tabName
}
