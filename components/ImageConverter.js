import React from 'react'
import PropTypes from 'prop-types'
import {
  downloadImageToBlob
} from 'utils/images'

class ImageConverter extends React.Component {
  constructor () {
    super()

    this.state = {
      blob: null
    }
  }

  componentDidMount () {
    const {
      url
    } = this.props

    downloadImageToBlob(url)
      .then(data => {
        if (!this.updater.isMounted(this)) return
        this.setState({
          blob: data.preview
        })
      })
  }

  render () {
    return this.props.children(this.state.blob)
  }
}

ImageConverter.propTypes = {
  url: PropTypes.string,
  children: PropTypes.func
}

export default ImageConverter
