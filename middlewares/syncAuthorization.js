import cookiesync from 'cookiesync'
import {
  LOGIN_USER,
  SIGN_OUT
} from 'platform/setup/actionTypes'
import config from 'platform/setup/config'
import { getLoggedInInfo } from 'platform/boarding/BoardingReducer'

let usersync

const syncAction = isLoggedIn => ({ isLoggedIn })

const handler = store => (value, old) => {
  if (!value || !old) {
    return
  }
  if (value.isLoggedIn !== old.isLoggedIn) {
    // some action for lgging in here as well?
    if (!value.isLoggedIn && getLoggedInInfo(store.getState())) {
      window.location = `${window.location.origin}${config.routes.signIn}`
    } else if (value.isLoggedIn && !getLoggedInInfo(store.getState())) {
      window.location = `${window.location.origin}${config.routes.candidate}`
    }
  }
}

const syncAuthorization = store => next => action => {
  if (!usersync || (usersync && !usersync.isRunning)) {
    usersync = cookiesync('user', syncAction, handler(store))
  }
  usersync.start(true)

  if (action.type === SIGN_OUT) {
    usersync.trigger(false)
  }

  if (action.type === LOGIN_USER) {
    usersync.trigger(true)
  }

  return next(action)
}

export default syncAuthorization
