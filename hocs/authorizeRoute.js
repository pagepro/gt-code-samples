import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { authorizeUser } from 'platform/boarding/BoardingActions'
import {
  COMPLETE,
  boardingRoutes
} from 'platform/setup/boardingStatuses'
import BigLoader from 'common/BigLoader'

const authorizeRoute = Component => {
  class AuthorizedRoute extends React.Component {
    componentDidMount () {
      const {
         history,
         location,
         isUserChecked,
         onBoardingStatus,
         isUserBoarded,
         authorizeUser,
         isUserLoggedIn
      } = this.props

      if (!isUserChecked) {
        authorizeUser()
          .then(boarding => {
            if (boarding.onBoardingStatus !== COMPLETE) {
              history.replace(boardingRoutes[boarding.onBoardingStatus])
            }
          })
      } else if (isUserLoggedIn && !isUserBoarded && location.pathname !== boardingRoutes[onBoardingStatus]) {
        history.replace(boardingRoutes[onBoardingStatus])
      }
    }

    render () {
      const {
        history,
        location,
        match,
        isUserChecked,
        onBoardingStatus
      } = this.props

      return isUserChecked && onBoardingStatus && Component
        ? (
          <div className='app'>
            <Component
              history={history}
              location={location}
              match={match}
            />
          </div>
        )
        : <BigLoader isLoading />
    }
  }

  AuthorizedRoute.propTypes = {
    authorizeUser: PropTypes.func,
    isUserChecked: PropTypes.bool,
    isUserLoggedIn: PropTypes.bool,
    isUserBoarded: PropTypes.bool,
    onBoardingStatus: PropTypes.string,
    history: PropTypes.object,
    match: PropTypes.object,
    location: PropTypes.object
  }

  const mapStateToProps = state => ({
    isUserChecked: state.boarding.checkedAuthorization,
    isUserLoggedIn: state.boarding.isLoggedIn,
    isUserBoarded: state.boarding.isBoarded,
    onBoardingStatus: state.boarding.step,
    userData: state.boarding.userData
  })

  return connect(mapStateToProps, {
    authorizeUser
  })(AuthorizedRoute)
}

export default authorizeRoute
