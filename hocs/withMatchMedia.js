import React from 'react'

const withMatchMedia = maxSize => EnhancedComponent => {
  const size = maxSize | 1040
  class MatchMedia extends React.Component {
    constructor (props) {
      super(props)

      this.tabletBreakpoint = window.matchMedia(`(max-width: ${size}px)`)

      this.state = {
        isTablet: this.tabletBreakpoint.matches
      }

      this.checksTabletMatchmedia = this.checksTabletMatchmedia.bind(this)
    }

    componentDidMount () {
      this.tabletBreakpoint.addListener(this.checksTabletMatchmedia)
    }

    componentWillUnmount () {
      this.tabletBreakpoint.removeListener(this.checksTabletMatchmedia)
    }

    checksTabletMatchmedia () {
      this.setState({
        isTablet: this.tabletBreakpoint.matches
      })
    }

    render () {
      return (
        <EnhancedComponent
          {...this.props}
          isTablet={this.state.isTablet}
        />
      )
    }
  }

  return MatchMedia
}

export default withMatchMedia
